var app;

app = new Marionette.Application;

app.addRegions({
  sideBar: '.sidebar-menu',
  content: '.page-content-container-js',
  modal: '.modal-body'
});

app.on('start', function() {
  var Example;
  Example = (function() {
    var that;
    that = {};
    that.init = function(options) {
      var elem;
      return elem = $(options.selector);
    };
    that.show = function(text) {
      clearTimeout(hideHandler);
      elem.find('span').html(text);
      return elem.delay(200).fadeIn().delay(4000).fadeOut();
    };
    return that;
  })();
  $.fn.serializeObject = function() {
    var a, o;
    o = {};
    a = this.serializeArray();
    $.each(a, function() {
      if (o[this.name] !== void 0) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        return o[this.name].push(this.value || '');
      } else {
        return o[this.name] = this.value || '';
      }
    });
    return o;
  };
  console.debug('app started');
  if (Backbone.history) {
    Backbone.history.start({
      pushState: true
    });
  }
  if (!window.setImmediate) {
    window.setImmediate = (function(_this) {
      return function() {
        var ID, head, onmessage, tail;
        head = {};
        tail = head;
        ID = Math.random();
        onmessage = function(e) {
          var func;
          if (e.data !== ID) {
            return;
          }
          head = head.next;
          func = head.func;
          delete head.func;
          return func();
        };
        if (window.addEventListener) {
          window.addEventListener('message', onmessage);
        } else {
          window.attachEvent('onmessage', onmessage);
        }
        return function(func) {
          tail = tail.next = {
            func: func
          };
          return window.postMessage(ID, "*");
        };
      };
    })(this)();
  }
  app.Parts.Filters.Controller.checkUserFilters();
  return app.Parts.Sidebar.Controller.show();
});

app.on('error', function(e) {
  return console.debug.apply(this, arguments);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Entities.Filters', function(Filters, app, Backbone, Marionette, $, _) {
  var API, initializeFilters;
  Filters.Items = (function(superClass) {
    extend(Items, superClass);

    function Items() {
      return Items.__super__.constructor.apply(this, arguments);
    }

    Items.prototype.localStorage = new Backbone.LocalStorage("Filters");

    Items.prototype.save = function() {
      return this.each(function(model) {
        return Backbone.localSync("update", model);
      });
    };

    return Items;

  })(Backbone.Collection);
  initializeFilters = function() {
    Filters.ItemList = new Filters.Items;
    return Filters.ItemList.fetch();
  };
  API = {
    getFilters: function() {
      if (Filters.ItemList === void 0) {
        initializeFilters();
      }
      return Filters.ItemList;
    }
  };
  return app.reqres.setHandler('get:filters', function() {
    return API.getFilters();
  });
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Entities.Menu', function(Menu, app, Backbone, Marionette, $, _) {
  var API, initializeMenu;
  Menu.Item = (function(superClass) {
    extend(Item, superClass);

    function Item() {
      return Item.__super__.constructor.apply(this, arguments);
    }

    return Item;

  })(Backbone.Model);
  Menu.Collection = (function(superClass) {
    extend(Collection, superClass);

    function Collection() {
      return Collection.__super__.constructor.apply(this, arguments);
    }

    Collection.prototype.model = Menu.Item;

    Collection.prototype.url = '/menu';

    Collection.prototype.parse = function(collection) {
      return collection.menu;
    };

    return Collection;

  })(Backbone.Collection);
  initializeMenu = function() {
    var defer, fetch, menu;
    menu = new Menu.Collection;
    defer = $.Deferred();
    fetch = menu.fetch();
    if (fetch) {
      fetch.done(function(res) {
        Menu.List = menu;
        if (res.code === 0) {
          return defer.resolveWith(fetch, [Menu.List]);
        } else {
          return defer.rejectWith(fetch, arguments);
        }
      });
      fetch.fail(function() {
        return defer.rejectWith(fetch, arguments);
      });
      return defer.promise();
    }
  };
  API = {
    getMenu: function() {
      var defer;
      if (Menu.List === void 0) {
        return initializeMenu();
      }
      defer = $.Deferred();
      setImmediate(function() {
        return defer.resolve(Menu.List);
      });
      return defer;
    }
  };
  return app.reqres.setHandler('get:menu', function() {
    return API.getMenu();
  });
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Entities.User', function(Users, app, Backbone, Marionette, $, _) {
  var API, initializeUsers;
  Users.Item = (function(superClass) {
    extend(Item, superClass);

    function Item() {
      return Item.__super__.constructor.apply(this, arguments);
    }

    Item.prototype.urlRoot = '/';

    Item.prototype.parse = function(model) {
      model.components = model;
      model.id = model.su_id;
      return model;
    };

    return Item;

  })(Backbone.Model);
  Users.Collection = (function(superClass) {
    extend(Collection, superClass);

    function Collection() {
      return Collection.__super__.constructor.apply(this, arguments);
    }

    Collection.prototype.url = '/users/all';

    Collection.prototype.model = Users.Item;

    Collection.prototype.parse = function(collection) {
      return collection.data;
    };

    return Collection;

  })(Backbone.Collection);
  initializeUsers = function(options) {
    var collection, defer, fetch;
    if (options == null) {
      options = {};
    }
    collection = new Users.Collection;
    defer = $.Deferred();
    fetch = collection.fetch(_.omit(options, 'success', 'error'));
    fetch.done(function(res) {
      defer.resolveWith(fetch, [collection, res]);
      return Users.tempCollection = collection;
    });
    fetch.fail(function() {
      return defer.rejectWith(fetch, arguments);
    });
    return defer.promise();
  };
  API = {
    getUsers: function(options) {
      return initializeUsers(options);
    }
  };
  app.reqres.setHandler('get:users', function(options) {
    return API.getUsers(options);
  });
  return app.reqres.setHandler('get:temp:users', function(options) {
    if (Users.tempCollection) {
      return Users.tempCollection;
    }
  });
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Pages', function(Pages, app, Backbone, Marionette, $, _) {
  var API, Router, api;
  Router = (function(superClass) {
    extend(Router, superClass);

    function Router() {
      return Router.__super__.constructor.apply(this, arguments);
    }

    Router.prototype.appRoutes = {
      '': 'showMain'
    };

    return Router;

  })(Marionette.AppRouter);
  API = (function(superClass) {
    extend(API, superClass);

    function API() {
      return API.__super__.constructor.apply(this, arguments);
    }

    API.prototype.showMain = function() {
      return Pages.Table.Controller.showPage();
    };

    return API;

  })(Marionette.Controller);
  api = new API;
  new Router({
    controller: api
  });
  return app.on('show:main', function() {
    app.navigate("/");
    return api.showMain();
  });
});

app.module('Sidebar', function(Sidebar, app, Backbone, Marionette, $, _) {

  /* TRIGGERS */
  return app.on('show:filter', function(view) {
    return app.Parts.Filters.Controller.show(view);
  });
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Parts.Mail', function(Mail, app, Backbone, Marionette, $, _) {
  return Mail.View = (function(superClass) {
    extend(View, superClass);

    function View() {
      return View.__super__.constructor.apply(this, arguments);
    }

    View.prototype.template = '#mail-tpl';

    View.prototype.onShow = function() {
      return $('#dialog').modal({
        "backdrop": "static",
        "keyboard": true,
        "show": true
      });
    };

    return View;

  })(Marionette.LayoutView);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Parts.Mail', function(Mail, app, Backbone, Marionette, $, _) {
  var controller;
  controller = (function(superClass) {
    extend(controller, superClass);

    function controller() {
      return controller.__super__.constructor.apply(this, arguments);
    }

    controller.prototype.show = function() {
      var view;
      view = new Mail.View;
      return app.modal.show(view);
    };

    return controller;

  })(Marionette.Controller);
  return Mail.Controller = new controller;
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Parts.Filters', function(Filters, app, Backbone, Marionette, $, _) {
  Filters.Item = (function(superClass) {
    extend(Item, superClass);

    function Item() {
      return Item.__super__.constructor.apply(this, arguments);
    }

    Item.prototype.template = '#filter-item-tpl';

    Item.prototype.onRender = function() {
      if (this.model.has('active') && this.model.get('active') === true) {
        return this.$('input').attr('checked', true);
      }
    };

    Item.prototype.events = {
      'change input': function() {
        if (this.model.has('active') && this.model.get('active') === true) {
          this.model.set('active', false);
          return this.model.save();
        } else {
          this.model.set('active', true);
          return this.model.save();
        }
      }
    };

    Item.prototype.modelEvents = {
      'change:active': function() {
        return app.Pages.Table.Controller.redrawPage();
      }
    };

    return Item;

  })(Marionette.ItemView);
  return Filters.View = (function(superClass) {
    extend(View, superClass);

    function View() {
      return View.__super__.constructor.apply(this, arguments);
    }

    View.prototype.template = '#filters-wrapper-tpl';

    View.prototype.childView = Filters.Item;

    View.prototype.childViewContainer = '.filters-container';

    View.prototype.onShow = function() {
      return $('#dialog').modal({
        "backdrop": "static",
        "keyboard": true,
        "show": true
      });
    };

    return View;

  })(Marionette.CompositeView);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Parts.Filters', function(Filters, app, Backbone, Marionette, $, _) {
  var controller;
  controller = (function(superClass) {
    extend(controller, superClass);

    function controller() {
      return controller.__super__.constructor.apply(this, arguments);
    }

    controller.prototype.show = function() {
      var filters, view;
      filters = app.request('get:filters');
      console.log(filters);
      view = new Filters.View({
        collection: filters
      });
      return app.modal.show(view);
    };

    controller.prototype.checkUserFilters = function() {
      var filters, filtesArr, i, item, len, results;
      filters = app.request('get:filters');
      filtesArr = [
        {
          'title': 'Фото',
          'name': 'user_f_116',
          'active': true
        }, {
          'title': 'Бейдж',
          'name': 'user_stiker',
          'active': true
        }, {
          'title': 'Фамилия',
          'name': 'user_f_103',
          'active': true
        }, {
          'title': 'Имя',
          'name': 'user_f_104',
          'active': true
        }, {
          'title': 'Отчество',
          'name': 'user_f_105',
          'active': true
        }, {
          'title': 'Е-mail',
          'name': 'su_email',
          'active': true
        }, {
          'title': 'Телефон',
          'name': 'su_pers_mob_phone',
          'active': true
        }, {
          'title': 'Серия паспорта',
          'name': 'user_f_173',
          'active': true
        }, {
          'title': 'Номер паспорта',
          'name': 'user_f_109',
          'active': true
        }, {
          'title': 'Код подразделения',
          'name': 'user_f_112',
          'active': true
        }, {
          'title': 'Кем выдан',
          'name': 'user_f_110',
          'active': true
        }, {
          'title': 'Дата выдачи',
          'name': 'user_f_111',
          'active': true
        }, {
          'title': 'Организация',
          'name': 'user_f_114',
          'active': true
        }
      ];
      if (filters.length !== filtesArr.length) {
        filters.each(function(oldItem) {
          return oldItem.destroy();
        });
        results = [];
        for (i = 0, len = filtesArr.length; i < len; i++) {
          item = filtesArr[i];
          results.push(filters.create(item));
        }
        return results;
      }
    };

    return controller;

  })(Marionette.Controller);
  return Filters.Controller = new controller;
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Parts.Sidebar', function(SideBar, app, Backbone, Marionette, $, _) {
  SideBar.MenuItem = (function(superClass) {
    extend(MenuItem, superClass);

    function MenuItem() {
      return MenuItem.__super__.constructor.apply(this, arguments);
    }

    MenuItem.prototype.template = '#sidebar-menu-item-tpl';

    MenuItem.prototype.tagName = 'li';

    MenuItem.prototype.className = 'nav-item';

    MenuItem.prototype.regions = {
      submenu: '.sub-menu'
    };

    MenuItem.prototype.events = {
      'click': function() {
        var trigger;
        trigger = this.model.get('trigger');
        return app.trigger(trigger, this);
      }
    };

    MenuItem.prototype.onRender = function() {
      return SideBar.Controller.showSubMenus(this);
    };

    return MenuItem;

  })(Marionette.LayoutView);
  return SideBar.View = (function(superClass) {
    extend(View, superClass);

    function View() {
      return View.__super__.constructor.apply(this, arguments);
    }

    View.prototype.childView = SideBar.MenuItem;

    View.prototype.tagName = 'ul';

    View.prototype.className = 'page-sidebar-menu';

    View.prototype.onShow = function() {
      if (this.options.sidebar) {
        this.$el.unwrap();
        return this.$el.removeClass('page-sidebar-menu').addClass('sub-menu');
      }
    };

    return View;

  })(Marionette.CollectionView);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Parts.Sidebar', function(SideBar, app, Backbone, Marionette, $, _) {
  var controller;
  controller = (function(superClass) {
    extend(controller, superClass);

    function controller() {
      return controller.__super__.constructor.apply(this, arguments);
    }

    controller.prototype.show = function() {
      var menu;
      menu = app.request('get:menu');
      menu.done(function(collection) {
        var view;
        view = new SideBar.View({
          collection: collection
        });
        app.sideBar.show(view);
        return console.log(collection);
      });
      return menu.fail(function(err) {});
    };

    controller.prototype.showSubMenus = function(view) {
      var collection, menuView;
      if (view.model.has('submenu')) {
        if (view.model.get('submenu').length) {
          collection = new Backbone.Collection(view.model.get('submenu'));
          menuView = new SideBar.View({
            collection: collection,
            sidebar: true
          });
          return view.submenu.show(menuView);
        }
      }
    };

    return controller;

  })(Marionette.Controller);
  return SideBar.Controller = new controller;
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Parts.User', function(User, app, Backbone, Marionette, $, _) {
  return User.ItemView = (function(superClass) {
    extend(ItemView, superClass);

    function ItemView() {
      return ItemView.__super__.constructor.apply(this, arguments);
    }

    ItemView.prototype.template = '#edit-person-tpl';

    ItemView.prototype.className = 'form-group';

    ItemView.prototype.events = {
      'keyup input': function() {
        var name;
        name = this.$el;
        return console.log(name);
      },
      'click .save-user': function() {
        var form;
        form = this.$('form').serializeObject();
        this.model.set(form);
        $.post('' + this.model.get('id'), form);
        return this.model.set('components', form);
      }
    };

    ItemView.prototype.onShow = function() {
      return $('#dialog').modal({
        "backdrop": "static",
        "keyboard": true,
        "show": true
      });
    };

    return ItemView;

  })(Marionette.ItemView);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Parts.User', function(User, app, Backbone, Marionette, $, _) {
  var controller;
  controller = (function(superClass) {
    extend(controller, superClass);

    function controller() {
      return controller.__super__.constructor.apply(this, arguments);
    }

    controller.prototype.showUserEditWindow = function(view) {
      var model;
      model = view.model;
      view = new User.ItemView({
        model: model
      });
      return app.modal.show(view);
    };

    return controller;

  })(Marionette.Controller);
  return User.Controller = new controller;
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Pages.Table', function(Table, app, Backbone, Marionette, $, _) {
  Table.Layout = (function(superClass) {
    extend(Layout, superClass);

    function Layout() {
      return Layout.__super__.constructor.apply(this, arguments);
    }

    Layout.prototype.template = '#users-table-layout-tpl';

    Layout.prototype.className = 'users-table-page';

    Layout.prototype.regions = {
      content: '.table-content',
      paginator: '.table-paginator'
    };

    return Layout;

  })(Marionette.LayoutView);
  Table.TableItemView = (function(superClass) {
    extend(TableItemView, superClass);

    function TableItemView() {
      return TableItemView.__super__.constructor.apply(this, arguments);
    }

    TableItemView.prototype.template = '#vendors-table-item-tpl';

    TableItemView.prototype.tagName = 'tr';

    TableItemView.prototype.events = {
      'click .send-mail': function() {
        return app.Parts.Mail.Controller.show();
      },
      'click .edit-user': function() {
        return app.Parts.User.Controller.showUserEditWindow(this);
      }
    };

    TableItemView.prototype.modelEvents = {
      'change': function() {}
    };

    return TableItemView;

  })(Marionette.ItemView);
  return Table.TableView = (function(superClass) {
    extend(TableView, superClass);

    function TableView() {
      return TableView.__super__.constructor.apply(this, arguments);
    }

    TableView.prototype.childView = Table.TableItemView;

    TableView.prototype.childViewContainer = 'tbody';

    TableView.prototype.template = '#vendors-table-tpl';

    TableView.prototype.onDomRefresh = function() {
      return app.Pages.Table.Controller.drawPaginate(window.PI);
    };

    return TableView;

  })(Marionette.CompositeView);
});

var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

app.module('Pages.Table', function(Table, app, Backbone, Marionette, $, _) {
  var controller;
  controller = (function(superClass) {
    extend(controller, superClass);

    function controller() {
      return controller.__super__.constructor.apply(this, arguments);
    }

    controller.prototype.showPage = function() {
      this.layout = new Table.Layout;
      app.content.show(this.layout);
      return this.showTable();
    };

    controller.prototype.showTable = function(page) {
      var fetch, self;
      if (page == null) {
        page = 1;
      }
      self = this;
      fetch = app.request('get:users', {
        data: {
          per_page: 50,
          page: page
        }
      });
      return fetch.done((function(_this) {
        return function(collection, PI) {
          var view;
          console.log(collection);
          window.PI = PI;
          view = new Table.TableView({
            collection: collection
          });
          return self.layout.content.show(view);
        };
      })(this));
    };

    controller.prototype.drawPaginate = function(PI) {
      var self;
      self = this;
      return $('#light-pagination').pagination({
        currentPage: PI.current_page,
        items: PI.total,
        itemsOnPage: PI.per_page,
        prevText: 'Назад',
        nextText: 'Вперед',
        selectOnClick: false,
        hrefTextPrefix: '/page',
        onPageClick: function(num, e) {
          e.preventDefault();
          return self.showTable(num);
        }
      });
    };

    controller.prototype.redrawPage = function() {
      var users, view;
      users = app.request('get:temp:users');
      view = new Table.TableView({
        collection: users
      });
      return this.layout.content.show(view);
    };

    controller.prototype.showUserEditWindow = function(view) {
      return console.log(view.model);
    };

    return controller;

  })(Marionette.Controller);
  return Table.Controller = new controller;
});
