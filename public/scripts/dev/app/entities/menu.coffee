app.module 'Entities.Menu', (Menu,app,Backbone,Marionette,$,_)->
  class Menu.Item extends Backbone.Model

  class Menu.Collection extends Backbone.Collection
    model: Menu.Item
    url:'/menu'
    parse: (collection)->
      return collection.menu

  initializeMenu = ->
    menu = new Menu.Collection
    defer = $.Deferred()
    fetch = menu.fetch();
    if fetch
      fetch.done (res)->
        Menu.List = menu
        if res.code is 0
          defer.resolveWith fetch,[Menu.List]
        else
          defer.rejectWith fetch,arguments
      fetch.fail ->
        defer.rejectWith fetch,arguments

      return defer.promise()


  API =
    getMenu: ->
      if Menu.List is undefined
        return initializeMenu()

      defer = $.Deferred()
      setImmediate ->
        defer.resolve Menu.List
      return defer


  app.reqres.setHandler 'get:menu', ->
    return API.getMenu()


