app.module 'Entities.User', (Users,app,Backbone,Marionette,$,_)->
  class Users.Item extends Backbone.Model
    urlRoot:'/'
    parse: (model)->
      model.components = model
      model.id = model.su_id
      return model

  class Users.Collection extends Backbone.Collection
    url:'/users/all'
    model: Users.Item
    parse: (collection)->
      return collection.data


  initializeUsers = (options = {})->
    collection = new Users.Collection
    defer = $.Deferred()
    fetch = collection.fetch(_.omit(options,'success','error'))
    fetch.done (res)->
      defer.resolveWith fetch,[collection,res]
      Users.tempCollection = collection
    fetch.fail ->
      defer.rejectWith fetch,arguments

    return defer.promise()



  API =
    getUsers: (options)->
      return initializeUsers options


  app.reqres.setHandler 'get:users', (options)->
    return API.getUsers options

  app.reqres.setHandler 'get:temp:users', (options)->
    if Users.tempCollection
      return Users.tempCollection
