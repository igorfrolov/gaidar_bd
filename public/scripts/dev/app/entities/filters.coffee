app.module 'Entities.Filters', (Filters,app,Backbone,Marionette,$,_)->
  class Filters.Items extends Backbone.Collection
    localStorage: new Backbone.LocalStorage "Filters"
    save: ->
      @each (model)->
        Backbone.localSync "update", model


  initializeFilters = ->
    Filters.ItemList = new Filters.Items

    do Filters.ItemList.fetch


  API =
    getFilters: ->
      if Filters.ItemList is undefined
        initializeFilters()

      return Filters.ItemList


  app.reqres.setHandler 'get:filters', ->
    return API.getFilters()


