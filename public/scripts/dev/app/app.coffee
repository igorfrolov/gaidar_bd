app = new Marionette.Application

app.addRegions
	sideBar:'.sidebar-menu'
	content: '.page-content-container-js'
	modal:'.modal-body'

app.on 'start', ->
	# info modal
	Example = do ->
		that = {}

		that.init = (options) ->
			elem = $(options.selector)

		that.show = (text) ->
			clearTimeout hideHandler
			elem.find('span').html text
			elem.delay(200).fadeIn().delay(4000).fadeOut()

		that

	# serialise form
	$.fn.serializeObject = ->
		o = {};
		a = this.serializeArray();
		$.each a, ->
			if (o[this.name] isnt undefined)
				if (!o[this.name].push)
					o[this.name] = [o[this.name]];
				o[this.name].push(this.value || '');

			else
				o[this.name] = this.value || '';

		return o;

	console.debug 'app started'
	if Backbone.history
		Backbone.history.start {pushState: true}

	if !window.setImmediate
		window.setImmediate = do =>
			head = { }
			tail = head

			ID = Math.random()

			onmessage = (e)->
				if e.data != ID
					return
				head = head.next
				func = head.func
				delete head.func
				func()

			if window.addEventListener
				window.addEventListener('message', onmessage);
			else
				window.attachEvent( 'onmessage', onmessage );


			return (func)->
				tail = tail.next = { func: func };
				window.postMessage(ID, "*")

	do app.Parts.Filters.Controller.checkUserFilters

	app.Parts.Sidebar.Controller.show()


app.on 'error', (e)->
  console.debug.apply @,arguments



