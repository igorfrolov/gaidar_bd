app.module 'Parts.Mail', (Mail,app,Backbone,Marionette,$,_)->
	class controller extends Marionette.Controller
		show: ->
			view = new Mail.View
			app.modal.show view

	Mail.Controller = new controller
