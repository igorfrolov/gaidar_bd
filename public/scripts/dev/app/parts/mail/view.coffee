app.module 'Parts.Mail', (Mail,app,Backbone,Marionette,$,_)->
	class Mail.View extends Marionette.LayoutView
		template: '#mail-tpl'
		onShow:->
			$('#dialog').modal
				"backdrop"  : "static",
				"keyboard"  : true,
				"show"      : true

