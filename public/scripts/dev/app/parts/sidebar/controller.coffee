app.module 'Parts.Sidebar', (SideBar,app,Backbone,Marionette,$,_)->
	class controller extends Marionette.Controller
		show: ->
			menu = app.request 'get:menu'

			menu.done (collection)->
				view = new SideBar.View
					collection: collection
				app.sideBar.show view
				console.log collection
			menu.fail (err)->

		showSubMenus: (view)->
			if view.model.has 'submenu'
				if view.model.get('submenu').length
					collection = new Backbone.Collection view.model.get('submenu')
					menuView = new SideBar.View
						collection: collection
						sidebar:true
					view.submenu.show menuView


	SideBar.Controller = new controller
