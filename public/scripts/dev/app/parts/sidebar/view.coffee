app.module 'Parts.Sidebar', (SideBar,app,Backbone,Marionette,$,_)->
	class SideBar.MenuItem extends Marionette.LayoutView
		template: '#sidebar-menu-item-tpl'
		tagName:'li'
		className:'nav-item'
		regions:
			submenu: '.sub-menu'
		events:
			'click': ->
				trigger = @model.get('trigger')
				app.trigger trigger,@

		onRender: ->
			SideBar.Controller.showSubMenus @


	class SideBar.View extends Marionette.CollectionView
		childView: SideBar.MenuItem
		tagName:'ul'
		className:'page-sidebar-menu'
		onShow:->
			if @options.sidebar
				this.$el.unwrap();
				@$el.removeClass 'page-sidebar-menu'
					.addClass 'sub-menu'
