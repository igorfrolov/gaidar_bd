app.module 'Parts.User', (User,app,Backbone,Marionette,$,_)->

  class User.ItemView extends Marionette.ItemView
    template:'#edit-person-tpl'
    className:'form-group'
    events:
      'keyup input':->
        name = @$el
        console.log name
      'click .save-user':->
        form = @$('form').serializeObject()
        @model.set(form)

        $.post ''+@model.get('id'),form
        @model.set('components',form)

    onShow:->
      $('#dialog').modal
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true

