app.module 'Parts.Filters', (Filters,app,Backbone,Marionette,$,_)->
	class Filters.Item extends Marionette.ItemView
		template: '#filter-item-tpl'
		onRender: ->
			if @model.has('active') and @model.get('active') is true
				@$('input').attr('checked',true)
		events:
			'change input': ->
				if @model.has('active') and @model.get('active') is true
					@model.set('active',false);
					@model.save();
				else
					@model.set('active',true);
					@model.save();

		modelEvents:
			'change:active': ->
				do app.Pages.Table.Controller.redrawPage

	class Filters.View extends Marionette.CompositeView
		template:'#filters-wrapper-tpl'
		childView:Filters.Item
		childViewContainer:'.filters-container'
		onShow:->
			$('#dialog').modal
				"backdrop"  : "static",
				"keyboard"  : true,
				"show"      : true



