app.module 'Parts.Filters', (Filters,app,Backbone,Marionette,$,_)->
	class controller extends Marionette.Controller
		show: ->
			filters = app.request 'get:filters'
			console.log filters
			view = new Filters.View
				collection: filters
			app.modal.show view

		checkUserFilters: ->
			filters = app.request('get:filters');
			filtesArr = [
					{'title':'Фото','name':'user_f_116','active':true},
					{'title':'Бейдж','name':'user_stiker','active':true},
					{'title':'Фамилия','name':'user_f_103','active':true},
					{'title':'Имя','name':'user_f_104','active':true},
					{'title':'Отчество','name':'user_f_105','active':true},
					{'title':'Е-mail','name':'su_email','active':true},
					{'title':'Телефон','name':'su_pers_mob_phone','active':true},
					{'title':'Серия паспорта','name':'user_f_173','active':true},
					{'title':'Номер паспорта','name':'user_f_109','active':true}
					{'title':'Код подразделения','name':'user_f_112','active':true},
					{'title':'Кем выдан','name':'user_f_110','active':true},
					{'title':'Дата выдачи','name':'user_f_111','active':true},
					{'title':'Организация','name':'user_f_114','active':true},

				]
			if filters.length isnt filtesArr.length
				filters.each (oldItem)->
					oldItem.destroy()

				for item in filtesArr
					filters.create item


	Filters.Controller = new controller
