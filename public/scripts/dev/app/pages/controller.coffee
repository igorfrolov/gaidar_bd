app.module 'Pages', (Pages,app,Backbone,Marionette,$,_)->
  class Router extends Marionette.AppRouter
    appRoutes:
      '':'showMain'

  class API extends Marionette.Controller
    showMain: ->
      do Pages.Table.Controller.showPage

  api = new API

  new Router
    controller:api

  app.on 'show:main', ->
    app.navigate "/"
    do api.showMain
