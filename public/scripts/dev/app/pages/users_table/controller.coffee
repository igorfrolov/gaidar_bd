app.module 'Pages.Table',(Table,app,Backbone,Marionette,$,_)->
  class controller extends Marionette.Controller
    showPage: ->
      @layout = new Table.Layout
      app.content.show @layout

      do @showTable

    showTable: (page=1)->
      self = @
      fetch = app.request 'get:users',data:{per_page:50,page:page}
      fetch.done (collection,PI) =>
        console.log collection
        window.PI = PI
        view = new Table.TableView
          collection:collection
        self.layout.content.show view


    drawPaginate: (PI)->
      self = @
      $('#light-pagination').pagination
        currentPage: PI.current_page
        items: PI.total,
        itemsOnPage: PI.per_page,
        prevText:'Назад'
        nextText:'Вперед'
        selectOnClick: false
        hrefTextPrefix: '/page'
        onPageClick: (num,e)->
          e.preventDefault()
          self.showTable num




    redrawPage: ->
      users = app.request('get:temp:users')
      view = new Table.TableView
          collection: users

      @layout.content.show view


    # user-edit
    showUserEditWindow:(view)->
      console.log view.model


  Table.Controller = new controller
