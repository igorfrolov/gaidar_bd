app.module 'Pages.Table', (Table,app,Backbone,Marionette,$,_)->
  class Table.Layout extends Marionette.LayoutView
    template: '#users-table-layout-tpl'
    className: 'users-table-page'
    regions:
      content: '.table-content'
      paginator: '.table-paginator'


  class Table.TableItemView extends Marionette.ItemView
    template:'#vendors-table-item-tpl'
    tagName:'tr'
    events:
      'click .send-mail': ->
        app.Parts.Mail.Controller.show()
      'click .edit-user':->
        app.Parts.User.Controller.showUserEditWindow @
    modelEvents:
      'change':->
        # do @render


  class Table.TableView extends Marionette.CompositeView
    childView:Table.TableItemView
    childViewContainer:'tbody'
    template:'#vendors-table-tpl'
    onDomRefresh:->
      app.Pages.Table.Controller.drawPaginate window.PI








