
 (function(){
 	$(document).on('click','.md-trigger',function(e){
 		e.preventDefault();
 		var t = $(this);
 		var modal = t.attr('data-modal');
 		openModal(modal);
 	})

 	$(document).on('click','.md-close,.md-overlay',function(){
 		closeModal();
 	})

 	openModal = function(modal){
 		$('#' + modal).addClass('md-show');
 		$('.md-overlay').addClass('overlay-show')
 	}

 	closeModal = function(){
 		$('.md-modal').removeClass('md-show');
 		$('.md-overlay').removeClass('overlay-show')
 	}


 })()