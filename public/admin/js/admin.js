(function(){
	//запрос всех пользователей из базы данных,для вывода в таблицу юзеров панели администрации
	$.ajax({
		url:'getAllUsers',
		type:'get',
	}).done(function(data){
		$('.loading-wrapper').hide();
		var data = JSON.parse(data);//Парсим список юзеров
		for (var i = 0; i < data.length; i++) {//цикл
			var user_table_template = ('<tr class="user_select">'+//шаблон одной строки таблицы юзеров
											'<td>'+(i+1)+'</td>'+
											'<td>'+data[i].first_name+'</td>'+
											'<td>'+data[i].sur_name+'</td>'+
											'<td>'+data[i].name+'</td>'+
											'<td>'+data[i].phone+'</td>'+
											'<td>'+data[i].email+'</td>'+
											'<td class="person_settings"></td>'+
											'<td class="settings_menu_wrapper">'+
												'<ul class="settings_menu" id="'+data[i].id+'" style="display:none;">'+
														'<li><a href="#" class="md-trigger update-user" data-modal="update-user">Редактировать</a></li>'+
														'<li><a href="#" class="md-trigger delete-user" data-modal="delete-user">Удалить</a></li>'+
												'</ul>'+
											'</td>'+
									 	 '</tr>');
			$('.users table tbody').append(user_table_template);//вставляем в тело таблицы
		};

	//запрос всех групп для  формы регистрации пользователя
		$.ajax({
			type:'get',
			url:'getAllGroups',
		}).done(function(data){
			var data = JSON.parse( data );
			for (var i = 0; i < data.length; i++) {
				var groups_user_table_template = ('<tr class="user_select">'+//шаблон одной строки таблицы групп
											'<td>'+(i+1)+'</td>'+
											'<td>'+data[i].name+'</td>'+
											'<td>'+data[i].Explane+'</td>'+
									 	 '</tr>');

				$('.users_groups table tbody').append(groups_user_table_template);//вставляем в тело таблицы
					
				$('.group_select').append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
				$('.groups-loading-wrapper').hide();
			};
			
})
		$.ajax({
		url:'getAllEvents',
		type:'get',
	}).done(function(data){
		$('.loading-wrapper').hide();
		var data = JSON.parse(data);//Парсим список юзеров
		for (var i = 0; i < data.length; i++) {//цикл
			var event_table_template = ('<tr class="event_select">'+//шаблон одной строки таблицы юзеров
											'<td>'+(i+1)+'</td>'+
											'<td>'+data[i].title+'</td>'+
											'<td>'+data[i].title_en+'</td>'+
											'<td>'+data[i].address+'</td>'+
											'<td>'+data[i].address_en+'</td>'+
											'<td>'+data[i].register_at+'</td>'+
											'<td>'+data[i].begin_at+'</td>'+
											'<td>'+data[i].end_at+'</td>'+
											'<td class="event_settings"></td>'+
											'<td class="settings_menu_wrapper">'+
												'<ul class="settings_menu" id="'+data[i].id+'" style="display:none;">'+
														'<li><a href="#" class="md-trigger update-event" data-modal="update-event">Редактировать</a></li>'+
														'<li><a href="#" class="md-trigger delete-event" data-modal="delete-event">Удалить</a></li>'+
												'</ul>'+
											'</td>'+
									 	 '</tr>');
			$('.events table tbody').append(event_table_template);//вставляем в тело таблицы
		};

		})
	});

})();

//запрос всех мероприятий из базы данных,для вывода в таблицу мероприятия панели администрации




//Менюшки,которые открываются по клику на иконку "механизма" в таблицах панели администрации
$(document).on('click','.users table tbody tr',function(){
	var t = $(this);
	var el = t.find('.settings_menu');
	if (el.is(':visible')) {
		$('.settings_menu').css('display','none');
	} else {
		$('.settings_menu').css('display','none');
		el.slideToggle('fast');
	}
	
})




//Редактирование пользователя

// Начинаем с того,что мы вытаскиваем данные о пользователе,на когорого выбрали для редактирования 
// и заполняем его данными поля формы редактирования
$(document).on('click','.update-user',function(){
	$('.absolute-wrapper').show();
	t = $(this);
	id = t.closest('.settings_menu').attr('id');
	$.ajax({
		type:'post',
		url:'getOneUser',
		data:{
			id:id
		}
	}).done(function(data){
		var data = JSON.parse(data);
		for (var i = 0; i < data.length; i++) {
			form = $('#update_user');
			form.find('input[name="id"]').attr('value',id)
			form.find('input[name="first_name"]').val(data[i].first_name);
			form.find('input[name="sur_name"]').val(data[i].sur_name);
			form.find('input[name="phone_number"]').val(data[i].phone);
			form.find('input[name="email"]').val(data[i].email);
		};
		$('.absolute-wrapper').hide();
	})
})

//После того,как мы заполнили форму редактирования данными о пользователе и проредактировали эти данные
//по клику сабмит запускаем функцию апдейта инфы о юзере в базе

$('#update_user').on('submit',function(ev){
	ev.preventDefault();
	var t = $( this );
	var inputs = t.serializeArray();
	console.log(inputs);
	var id  = t.closest('#update_user').attr('data-id');
	$.ajax({
		type:'post',
		url:'register/update_user',
		data:inputs
	})

}) 



//Добавление пользователя
$('#reg_user_form').on('submit',function(ev){
	ev.preventDefault();
	var inputs = $(this).serializeArray();
	$.ajax({
		type:'post',
		url:'registration_user',
		data:inputs
	}).done(function(){
			console.log('done');
		})

}) 



//Удаление пользователя
	$(document).on('click','.delete-user',function(){
		t = $(this);
		id = t.closest('.settings_menu').attr('id');
		$('.md-delete').attr('data-id',id);
	})

	//нажал 'нет'-окно закрылось
	$('.delete_declined').click(function(){
		$('.md-modal').removeClass('md-show');
 		$('.md-overlay').removeClass('overlay-show');
	})

	//нажал 'да'-собственно,удаление
	$('.delete_accepted').click(function(){
		t = $(this);
		id = t.closest('.md-delete').attr('data-id');
		$.ajax({
			type:'post',
			url:'register/delete_user',
			data:{
				id:id
			}
		}).done(function(){
			$('.md-delete>.md-content').empty().html('<h3>Пользователь удалён успешно,пожалуйста,обновите страницу</h3>');
		})
	})	
//

