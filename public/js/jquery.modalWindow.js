(function( $ ) {
	$(document).ready( function(){
		var trigger = '';
		
		//Создаем модальное окно
		function Modal(image) {
			
			OverlayDelete();
			$('body').append($('<div>').addClass('modal-container'));
			$('.modal-container').append($('<div>').addClass('close-button-panel').append('<span class="close"></span>'));
			
			if(image === 'upload'){
				$('.modal-container').prepend($('<div>').append(
					'<form action="" method="GET">'+
						'<div class="upload-frame"><div class="upload-frame-label">Нажмите чтобы выбрать файл<br /><span>... или перетащите его мышью в окошко</span></div></div>'+ 
						'<input id="upload-submit" type="file" accept="image/jpeg, image/png, image/gif" />'+
					'</form>'
				).addClass('modal-content'));

			} else {
				$('.modal-container').prepend(	$('<div>').html(content).addClass('modal-content')	);
			}
		
			height = $('.modal-container').height() + 'px';
			
			$('.modal-container').css({height: 0, position: 'fixed', margin: 'auto', top: 0, bottom: 0, left: 0, right: 0});
			$('.modal-container').css({height: height, position: 'fixed', margin: 'auto', top: 0, bottom: 0, left: 0, right: 0});
			
		}
		
		//Создаем наложение
		function OverlayCreate() {
			$('body').append($('<div>').addClass('overlay'));
		}
		
		//Удаляем модальное окно
		function OverlayDelete() {
			$('.modal-container').remove();
			$('.overlay').remove();
		}
		
		$('body').on('click', '.close-button-panel', function() {
			 OverlayDelete();
		});
		
		$('body').on('click', '.overlay', function() {
			OverlayDelete();
		});
		
		$(trigger).click( function(){
			if(trigger !== ''){
				Modal();
				OverlayCreate();
			}
		});
		
		$('body').on('click', '.upload',  function() {
			Modal('upload');
			OverlayCreate();
		});
		
		$('body').on('click','.upload-frame', function() {
			$("#upload-submit").trigger('click');
		});
		
		$('body').on('onmouseup', '.upload-frame', function() {
			$("upload-submit").trigger('click');
		});
	});
})(jQuery);