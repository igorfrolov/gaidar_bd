( function($) {
	$(document).ready( function(){
	
	
	
	$('#accept, #autochk').removeAttr("checked");
	
	/*===Отключаем автозавершение ввода у всех input'ов===*/
	
	$('.row input[type=text]').attr('autocomplete', 'off');	
	
	/*===============================================================================*/
	
	/*===================Скрытие/показ сообщений=========================*/
	
	function hideMsg(obj) {
		$(obj).siblings('.error-msg-block').css({'display' : 'none'})
			.siblings('.alert-msg-block').css({'display' : 'none'});
	}
	
	function showMsg(obj) {
		$(obj).siblings('.error-msg-block').css({'display' : 'block'})
			.siblings('.alert-msg-block').css({'display' : 'block'});
		
	}

	$('body').on('blur', 'input[type=text]', function(){ hideMsg($(this)) });
	$('body').on('focus', 'input[type=text]', function(){ showMsg($(this)) });
		
	/*===============================================================================*/
	
	
	$(document).on('click', '#man', function(event){
		event.preventDefault();
		
		$('#man-block').show();
		
		$(this).text('Закрыть');
		$(this).addClass('openede');
		
	});
	
	$(document).on('click', '.openede', function(event) {
		event.preventDefault();
		
		$(this).removeClass('openede');
		$(this).text('Воспользуйтесь ручной формой ввода.');
		$('#man-block').hide();
	});
	
	//*=============Выводим сообщение об ошибке=======================*/
	function errorMsg(obj, text) {
		$(obj).removeClass('valid-text-input');
		$(obj).addClass('error-text-input');
		$(obj).siblings('.error-msg-block').remove();
		$(obj).after($('<div/>', {
						class: 'error-msg-block',
						text: text
					}).append('<div/>'));
		$(obj).parent('.row').css({'position' : 'relative'});
	}
	/*===============================================================================*/
	
	/*================Оповещаем об успешно введенном инпуте==========================*/
	function validAlert(obj) {
		$(obj).removeClass('error-text-input');
		$(obj).addClass('valid-text-input');
		$(obj).siblings('.error-msg-block').remove();
	}
	/*===============================================================================*/
	
	/*=====================Удаляем все оповещения===================================*/
	function alertClear(obj) {
		$(obj).removeClass('valid-text-input');
		$(obj).removeClass('error-text-input');
		$(obj).siblings('.error-msg-block').remove();
	}
	
	/*===============================================================================*/
	
	
	
	
	/*===Проверка на введенные буквы русского алфавита #surname, #name, #patronymic===*/
	
	$('body').on('keyup', '#surname, #name, #patronymic', function(){
		var obj = $(this);
		var value  = $(this).val();
		var regexp = /[^а-яА-ЯёЁ -]/;
		var text = 'Используйте буквы русского алфавита';
		
		if(regexp.test(value) && value.length) {
			errorMsg(obj, text);
			
		} else if(!regexp.test(value) && value.length) {
			validAlert(obj);
			$('.acceptblock').show();
		$('.acceptblock').html('<h2>Согласие на обработку персональных данных</h2>' +
        '<p>Настоящее Согласие на обработку персональных данных (далее – «Согласие») регулирует отношения между ООО «Альянс Медиа Стратегия» (далее – «Оператор») и физическим лицом ' + $('#surname').val() + ' ' + $('#name').val() + ' ' + $('#patronymic').val() + ' ' + '(далее – «Пользователь») в области предоставления и обработки персональных данных Пользователя.</p>'+
        '<p>Обработка персональных данных производится в соответствии с Федеральным законом от 27.07.2006 № 152-ФЗ «О персональных данных» (далее – «Закон»).</p>'+
        '<p>1. Пользователь вносит свои персональные данные на интернет-сайт www.gaidarforum.ru путем заполнения онлайн-заявки, а Оператор производит обработку персональных данных Пользователя как с использованием средств автоматизации, так и без их использования.</p>'+
        '<p>2. Пользователь принимает решение о предоставлении своих персональных данных и дает согласие на их обработку своей волей и в своем интересе, за исключением случаев, определенных ч. 2 ст. 9 Закона.'+
        '<p>3. Пользователь несет ответственность за предоставление персональных данных иного лица.</p>'+
        '<p>4. Согласие вступает в силу с момента его подтверждения Пользователем на сайте www.gaidarforum.ru посредством проставления отметки в графе «Согласен».</p>'+
        '<p>5. Цель обработки персональных данных: регистрация участника на Форуме.</p>'+
        '<p>6. Пользователь соглашается с тем, что Оператор имеет право на хранение и обработку, в том числе и автоматизированную, любой информации, относящейся к персональным данным Пользователя в соответствии с Законом, включая сбор, систематизацию, накопление, хранение, уточнение, использование, распространение (в том числе передачу), обезличивание, уничтожение персональных данных, предоставленных Пользователем.</p>'+
        '<p>7. Согласие может быть отозвано Пользователем в его личном кабинете на интернет-сайте www.gaidarforum.ru.</p>'+
        '<p>8. Перечень персональных данных, на обработку которых Пользователь дает согласие: фамилия, имя, отчество; дата рождения; место рождения; адрес; гражданство; фотография; пол; номер контактного телефона; марка автомобиля, государственный номер; фамилия, имя, отчество водителя; контактное лицо; адрес электронной почты; паспортные данные: вид документа, серия и номер документа, орган, выдавший документ, наименование, код, дата выдачи документа.</p>'+
        '<p>9. Пользователь настоящим подтверждает, что все указанные им данные принадлежат ему лично.</p>'+
        '<p>10. Пользователь подтверждает и признает, что он внимательно и в полном объеме прочитал Согласие и условия обработки Оператором персональных данных, указываемых им в онлайн-заявке; текст Согласия и условия обработки персональных данных ему понятны.</p>');
			
		} else {
			alertClear(obj);
		}
		
		
	});
	
	/*===============================================================================*/
	
	/*==================При фокусе на #surname, #name, #patronymic подсветка подсказки по транслитерации===================*/
	
	$('body').on('focus', '#surname, #name, #patronymic', function(){
		var id = $(this).attr('id');
		var value  = $(this).val();
		var translval = $('#latin-' + id);
		 
		if(translval.hasClass('alert-text-input')) {
			$(translval).siblings('.alert-msg-block').css({'display' : 'block'});
		}
	});
	
	/*===============================================================================*/
	
	/*==========Изменение регистра букв #surname, #name, #patronymic=================*/
	
	$('body').on('keypress', '#surname, #name, #patronymic', function(){
		
		var val = '';
		var id = $(this).attr('id');
		var value  = $(this).val();
		var translval = $('#latin-' + id);
		var regexp = / /;
		var regexp2 = /\-/;
		
		if(regexp.test(value)){
		
		var splitValue = value.split(' ');
			for(i = 0; i <= (splitValue.length - 1); i++) {
				if(i !== 0) {
					val += ' ' + splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
				} else {
					val += splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
				}
			}
		} else if(regexp2.test(value)) {
		
			var splitValue = value.split('-');
			for(i = 0; i <= (splitValue.length - 1); i++) {
				if(i !== 0) {
					val += '-' + splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
				} else {
					val += splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
				}
			}
		} else {
			val = value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
		}
		
		$(this).val(val);
		

		
	});
	
	
	/*===============================================================================*/
	
	/*================Прячем подсказку #latin-surname, #latin-name, если потеряли фокус #surname, #name==============*/
	$('body').on('blur', '#surname, #name', function(){
		
		var id = $(this).attr('id');
		var translval = $('#latin-' + id);
		
		$(translval).siblings('.alert-msg-block').css({'display' : 'none'});
		
	});
	
	
	/*===============================================================================*/
	
	/*==============Транслитерация дублирующихся значений #surname, #name============*/
	$('body').on('keyup', '#name, #surname', function() {
		
		var id = $(this).attr('id');
		var value = $(this).val();
		var translval = $('#latin-' + id);
		
		if(value.length && !$(this).hasClass('error-text-input')) {
			$(translval).addClass('alert-text-input');
			$(translval).siblings('.alert-msg-block').remove();
			$(translval).after($('<div/>', {
						class: 'alert-msg-block',
						text: 'Отредактируйте, если есть неточности'
					}).append('<div/>'));
					
			$(translval).parent('.row').css({'position' : 'relative'});
			
			translit(id, value);
			
		} else {
			$(translval).removeClass('alert-text-input');
			$(translval).siblings('.alert-msg-block').remove();
		}
		
		
		function translit(id, value){
			var value = value.toLowerCase();
			// Символ, на который будут заменяться все спецсимволы
			var space = ''; 

			// Массив для транслитерации
			var transl = {
				'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh', 
				'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
				'о': 'o', 'п': 'p', 'р': 'r','с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'kh',
				'ц': 'ts', 'ч': 'ch', 'ш': 'sh', 'щ': 'shch','ъ': space, 'ы': 'y', 'ь': space, 'э': 'e', 'ю': 'yu', 'я': 'ya',
				' ': space, '_': space, '`': space, '~': space, '!': space, '@': space,
				'#': space, '$': space, '%': space, '^': space, '&': space, '*': space, 
				'(': space, ')': space,'-': space, '\=': space, '+': space, '[': space, 
				']': space, '\\': space, '|': space, '/': space,'.': space, ',': space,
				'{': space, '}': space, '\'': space, '"': space, ';': space, ':': space,
				'?': space, '<': space, '>': space, '№':space,
			}

			var result = '';
			var curent_sim = '';

			for(i=0; i < value.length; i++) {
				// Если символ найден в массиве то меняем его
				if(transl[value[i]] != undefined) {
					if(curent_sim != transl[value[i]] || curent_sim != space){
						 result += transl[value[i]];
						 curent_sim = transl[value[i]];
					}                                                                             
				}
				// Если нет, то оставляем так как есть
				else {
					result += value[i];
					curent_sim = value[i];
				}
			}
			
			result = TrimStr(result);               
			
			
			regexp = / /;
			regexp2 = /-/;
			
			var value  = result;
			var val = '';
			
			if(regexp.test(value)){
			
			var splitValue = value.split(' ');
				for(i = 0; i <= (splitValue.length - 1); i++) {
					if(i !== 0) {
						val += ' ' + splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
					} else {
						val += splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
					}
				}
			} else if(regexp2.test(value)) {
				var splitValue = value.split('-');
				for(i = 0; i <= (splitValue.length - 1); i++) {
					if(i !== 0) {
						val += '-' + splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
					} else {
						val += splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
					}
				}
			} else {
				val = value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
			}
			
			$(translval).val(val); 
		}
		
		function TrimStr(s) {
			s = s.replace(/^-/, '');
			return s.replace(/-$/, '');
		}
		
		
	});
	/*===============================================================================*/
	
	/*=====Проверка на введенные буквы англйиского алфавита  #latin-surname, #latin-name====*/
	$('body').on('keyup', '#latin-surname, #latin-name', function(){
		
		var obj = $(this);
		var value  = $(this).val();
		var regexp = /[^a-zA-Z- ]/;
		var text = 'Используйте буквы английского алфавита';

		if(regexp.test(value) && value.length) {
			$(this).removeClass('alert-text-input');
			$(this).siblings('.alert-msg-block').remove();
			errorMsg(obj, text);
			
		} else if(!regexp.test(value) && value.length) {
			$(this).removeClass('alert-text-input');
			$(this).siblings('.alert-msg-block').remove();
			validAlert(obj);
			
		} else {
			$(this).removeClass('alert-text-input');
			$(this).siblings('.alert-msg-block').remove();
			alertClear(obj);
		}
		
	});
	
	/*===============================================================================*/
	
	
	/*=====Проверка на введенные буквы русского алфавита  #organization, #position====*/
	
	$('body').on('keyup','#organization, #position', function() {
		var regexp = /[^а-яА-ЯёЁ -\.№0-9"]/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Используйте буквы русского алфавита'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
	});
	
	/*===============================================================================*/
	
	/*=====Проверка на введенные буквы англйиского алфавита  #latin-organization, #latin-position====*/
	$('body').on('keyup', '#latin-organization, #latin-position', function(){

		var regexp = /[^a-zA-Z -\.№0-9"]/;
		var value  = $(this).val();
	
		
	
		if(regexp.test(value) && $(this).val().length) {
			
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Используйте буквы английского алфавита'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).addClass('valid-text-input');
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
		
		
	});
	
	/*===============================================================================*/
	
	/*===========================Проверка отмеченого radio===========================*/

	$('body').on('click', '.jq-radio', function(){
		
		if($(this).hasClass(".checked")){
			$(this).parent('.row').removeClass('error-radio-input');
			$(this).parent('.row').addClass('valid-radio-input');
		}
	});
	
	/*===============================================================================*/
	
	/*===================Получение списка городов, при выборе области=====================*/
	
	$(document).on('click', '.region .jq-selectbox__dropdown li.selected', function(){
		
		val = $(this).html();
			
		$('select[name="user_f_108"]').html('');
		$('.city').find('.error-msg-block').remove();

		$.post(
			'php/select_city.php', {
			"select_val":  val
			}, function(data) {
				$('select[name="user_f_108"]').append('<option></option>' + data);
				
				setTimeout(function() {  
					  $('select[name="user_f_108"]').trigger('refresh');  
					}, 1);
			})

	});
		
	/*===============================================================================*/
	
	
	/*===================Проверка на корректные символы #issued=====================*/
	$('body').on('keyup', '#issued', function() {
		var value = $(this).val();
		
		var regexp = /[^а-яА-ЯёЁ \-\.№0-9]/;
		var value  = $(this).val();
		
		
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Используйте буквы русского алфавита'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
		
	});
	
	/*===============================================================================*/
	
	/*===================Проверка на корректные символы #pbirth=====================*/
	$('body').on('keyup', '#pbirth', function() {
		var value = $(this).val();
		var regexp = /[^а-яА-ЯёЁ -\.№0-9]/;
		
		
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Используйте буквы русского алфавита'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
		
	});
	
	/*===============================================================================*/
	
	/*=========================Проверка #passportS===============================*/

	$('body').on('keyup', '#passportS', function(){
	
		var regexp = /[0-9]{2} [0-9]{2}/;
		var defvar = /__ __/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).addClass('valid-text-input');
			$('#passportN').trigger('focus');
			
		} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Введите 4 цифры серии паспорта по формату "XX XX"'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		}
	
	});
	
	/*===============================================================================*/
	
	/*=========================Проверка #passportN==============================*/

	$('body').on('keyup', '#passportN', function(){
	
		var regexp = /[0-9]{6}/;
		var defvar = /______/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Введите 6 цифр номера паспорта по формату "XXXXXX"'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		
		}
	});
	
	/*===============================================================================*/
	
	
	/*=========================Проверка #code========================================*/
	
	$('body').on('keyup', '#code', function(){
	
		var regexp = /[0-9]{3}-[0-9]{3}/;
		var regexp2 = /^[0-9]-/
		var defvar = /___-___/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			
		} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Введите 6 цифр кода подразделения по формату "XXX-XXX"'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
		
		}
	
	});
	
	/*===============================================================================*/
	
	
	// Проверка на корректность e-mail адреса
	$('body').on('keyup', 'input[type=text]#email', function(){
		
		var regexp = /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/;
		var value  = $(this).val();
		
		if(regexp.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
			
		} else if(!regexp.test(value) && $(this).val().length) {
		$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Введите e-mail по формату: email@domain.ru'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
		
	});
	
	
	// Проверка введенной даты
	$('body').on('keyup', '#date, #date-b', function(){
	
		var regexp = /(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d/;
		var defvar = /__.__.____/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			
		} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
		
		}
	
	});
	
	$(document).on('focus', '.city .jq-selectbox', function(){
		
			if($(this).find('li').text() === '') {
				
				$('.city .jq-selectbox').after($('<div/>', {
							class: 'error-msg-block',
							text: 'Сначала выберите регион'
						}).append('<div/>'));
			}
		});
	
	// Проверка введенной даты
	$('body').on('change', '#date, #date-b', function(){
	
		var regexp = /(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d/;
		var defvar = /__.__.____/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			
		} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
		
		}
	
	});
	
	/*$('body').on('keyup', '#phone', function(){
		var regexp = /\+7 \([0-9]{3}\) [0-9]{3}-[0-9]{4}/;
		var defvar = /\+7 \(___\) ___-____/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length && !regexp.test(defvar)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
			} else if(!regexp.test(value) && $(this).val().length && !regexp.test(defvar)) {
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Введите 10 цифр телефонного номера'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
			} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		
		}
	});*/
	
	$('#marka, #gos, #transport, #photoapp, #videoapp').keyup( function() {
		if($(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).addClass('valid-text-input');
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
	});
	
	
	$('#contact, #fiodr').keyup( function() {
		var regexp = /[^а-яА-ЯёЁ ]/;
		var value  = $(this).val();
		
		
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Используйте буквы русского алфавита'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
	});
	
	$('.jq-file__name').change( function(){
		if($(this).text() == 'Файл не выбран') {
			$('.file').addClass('error-text-input');
			$('.file').append($('<div/>', {
						class: 'error-msg-block',
						text: 'Выберите изображение для загрузки'
					}).append('<div/>'));
			$('.file').css({'position' : 'relative'});
		} else {
			$('.file').removeClass('error-text-input');
			$('.file').children('.error-msg-block').remove();
		}
		
	});	
	
	$(document).on('focus', '.region .jq-selectbox', function() {
			if($(this).find('.jq-selectbox__select-text').hasClass('error-text')) {
				$(this).find('.jq-selectbox__select-text').removeClass('error-text');
				$(this).find('.error-msg-block').remove();
			}
		
	});
	
	$(document).on('keyup', '#mob', function() {
		var regexp = /((8|\+7)[\- ]?)?(\(?\d{3,5}\)?[\- ]?)?[\d\- ]{7,10}$/;
		var value  = $(this).val();
		
		
			if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
				
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							html: 'Введите номер телефона по формату: <br /> +7 (903) 900 0000'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
			} else {
				$(this).removeClass('valid-text-input');
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
		
	});
	
	// Проверка значений формы перед отправкой
	$('body').on('click', 'input[type=submit]', function (event) {
		if($('div').is('.error-msg-box')) {
			$('.error-msg-box').remove();
		}
		
		$('.row input[type=text]:not(#marka, #gos, #fiodr, #phone, #adregion, #adcity)').each( function(index){
			
			if(!$(this).val().length) {
				event.preventDefault();
				$(this).addClass('error-text-input');
				
				$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Поле не может быть пустым!'
				}).append('<div/>'));
					
				$(this).parent('.row').css({'position' : 'relative'});
			} else {
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
			
		});
		
		
	$('#surname, #name, #patronymic').each( function(index) {
			
			var regexp = /[^а-яА-ЯёЁ -]/;
			var value  = $(this).val();

			if(regexp.test(value) && $(this).val().length) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Используйте буквы русского алфавита'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
		});
		
		
		$('#issued, #pbirth').each( function(index) {
			
			var regexp = /[^а-яА-ЯёЁ -\.№0-9]/;
			var value  = $(this).val();

			if(regexp.test(value) && $(this).val().length) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Используйте буквы русского алфавита'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
		});
		
		
		$('#fiodr, #contact').each( function(index) {
			
			var regexp = /[^а-яА-ЯёЁ ]/;
			var value  = $(this).val();

			if(regexp.test(value) && $(this).val().length) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Используйте буквы русского алфавита'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
		});
		
		$('.row #passportS').each( function(index){
	
			var regexp = /[0-9]{2} [0-9]{2}/;
			var defvar = /__ __/;
			var value  = $(this).val();
			
			if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
				
			} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Введите 4 цифры серии паспорта по формату "XX XX"'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
			} 
	
		});
		
		
			$('#latin-surname, #latin-name').each( function(index){
			var regexp = /[^a-zA-Z- ]/;
			var value  = $(this).val();

			if(regexp.test(value) && $(this).val().length) {
				event.preventDefault();
				$(this).removeClass('alert-text-input');
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Используйте буквы английского алфавита'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('alert-text-input');
				$(this).addClass('valid-text-input');
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				
			}
		});
		
	$('.row input[type=text]#email').each( function(index){
		
		var regexp = /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/;
		var value  = $(this).val();
		
		if(regexp.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
					
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Введите e-mail по формату: email@domain.ru'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});

		} 
	});
	
	$('#mob').each( function() {
		var regexp = /((8|\+7)[\- ]?)?(\(?\d{3,5}\)?[\- ]?)?[\d\- ]{7,10}$/;
		var value  = $(this).val();
		
		if($('#phone').val() !== '') {
			if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
				
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							html: 'Введите номер телефона по формату: <br /> +7 (903) 900 0000'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
			} 
		}

		
	});
		
		$('.row .jq-selectbox__select-text').each( function(index) {
			if(!$('a').is('.openede')) {
				if($(this).text() === 'Выберите...') {
					event.preventDefault();
					$(this).addClass('error-text');
					$(this).after($('<div/>', {
								class: 'error-msg-block',
								text: 'Выберите значение из списка'
							}).append('<div/>'));
					$(this).parent('.row').css({'position' : 'relative'});
					
				} else {
					$(this).removeClass('error-text');
					$(this).siblings('.error-msg-block').remove();
				}
			} else {
				$(this).removeClass('error-text');
					$(this).siblings('.error-msg-block').remove();
			}
		});
		
		if(!$('#autochk-styler').hasClass('checked')){
			$('#marka, #gos, #fiodr').each( function(index){
				if(!$(this).val().length) {
					event.preventDefault();
					$(this).addClass('error-text-input');
					
					$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Поле не может быть пустым!'
					}).append('<div/>'));
						
					$(this).parent('.row').css({'position' : 'relative'});
					
				}
			
			});
		} else {
			$('#marka, #gos, #fiodr').each( function(index){
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			});
		}
		
		/*$('.visite input[type="checkbox"]').each( function() {
			var checked = '';
			if($(this).prop('checked')){
				checked = 'true';
			}
			console.log(checked);
			return checked;
			
		});
		if(!checked.length) {
				event.preventDefault();
				$(this).addClass('error-text-input');
				$('.visite').append($('<div/>', {
							class: 'error-msg-block',
							text: 'Выберите хотя бы один день для посещения'
					}).append('<div/>'));
			}
		*/
		$('#date, #date-b').each( function(index){
	
			var regexp = /(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d/;
			var defvar = /__.__.____/;
			var value  = $(this).val();
			
			if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');

				
			} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Неверно указана дата'
					}).append('<div/>'));
						
					$(this).parent('.row').css({'position' : 'relative'});
					
			} 
	
		});
		
		
		$('.jq-file__name').each( function(index){
			if($(this).text() === 'Файл не выбран') {
				event.preventDefault();
				$('.file').addClass('error-text');
				$('.file').append($('<div/>', {
							class: 'error-msg-block',
							text: 'Выберите изображение для загрузки'
						}).append('<div/>'));
				$('.file').css({'position' : 'relative'});
				
			} else {
				$('.file').removeClass('error-text');
				$('.file').children('.error-msg-block').remove();
			}
			
		});	
		
		$('#organization, #position').each( function() {
		var regexp = /[^а-яА-ЯёЁ -\.№0-9]/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Используйте буквы русского алфавита'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		}
	});
		
	$('#latin-organization, #latin-position').each( function(){

		var regexp = /[^a-zA-Z -\.№0-9]/;
		var value  = $(this).val();
	
		if(regexp.test(value) && $(this).val().length) {
			
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Используйте буквы английского алфавита'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).addClass('valid-text-input');
			
		} 
		
		
	});
	

	
		if($('.row input').is('.error-text-input')) {
			event.preventDefault();
			$('.error-msg-block').css({'display' : 'none'});
			
			$('.content-block form').prepend($('<div>', {
				class: 'error-msg-box',
				html: '<span>Не удалось отправить форму</span><br />Исправьте некорректно заполненные поля, помеченные красным крестом, руководствуясь подсказкой под полем'
			}));
			
			var scrollTop = $('.error-msg-box').offset().top - 10;
			$(document).scrollTop(scrollTop);
		}
		
	});	
	

	});
	

})(jQuery)