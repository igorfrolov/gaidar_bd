( function($) {
	$(document).ready( function(){
	
	/*===Отключаем автозавершение ввода у всех input'ов===*/
	
	$('.row input[type=text]').attr('autocomplete', 'off');	
	
	/*===============================================================================*/
	
	/*===================Скрытие/показ сообщений=========================*/
	
	function hideMsg(obj) {
		$(obj).siblings('.error-msg-block').css({'display' : 'none'})
			$(obj).siblings('.alert-msg-block').css({'display' : 'none'});
	}
	
	function showMsg(obj) {
		$(obj).siblings('.error-msg-block').css({'display' : 'block'})
			$(obj).siblings('.alert-msg-block').css({'display' : 'block'});
		
	}
	
	$('body').on('focusout', 'input[type=text]', function(){ hideMsg($(this)) });
	$('body').on('focus', 'input[type=text]', function(){ showMsg($(this)) });
		
	/*===============================================================================*/
	
	
	
	
	//*=============Выводим сообщение об ошибке=======================*/
	function errorMsg(obj, text) {
		$(obj).removeClass('valid-text-input');
		$(obj).addClass('error-text-input');
		$(obj).siblings('.error-msg-block').remove();
		$(obj).after($('<div/>', {
						class: 'error-msg-block',
						text: text
					}).append('<div/>'));
		$(obj).parent('.row').css({'position' : 'relative'});
	}
	/*===============================================================================*/
	
	/*================Оповещаем об успешно введенном инпуте==========================*/
	function validAlert(obj) {
		$(obj).removeClass('error-text-input');
		$(obj).addClass('valid-text-input');
		$(obj).siblings('.error-msg-block').remove();
	}
	/*===============================================================================*/
	
	/*=====================Удаляем все оповещения===================================*/
	function alertClear(obj) {
		$(obj).removeClass('valid-text-input');
		$(obj).removeClass('error-text-input');
		$(obj).siblings('.error-msg-block').remove();
	}
	
	/*===============================================================================*/
	
	
	
	
	/*===Проверка на введенные буквы русского алфавита #surname, #name, #patronymic===*/
	
	$('body').on('keyup', '#surname, #name, #patronymic', function(){
		var obj = $(this);
		var value  = $(this).val();
		var regexp = /[^a-zа-яёЁ -]/i;
		var text = 'Use the letters of the English alphabet';
		
		if(regexp.test(value) && value.length) {
			errorMsg(obj, text);
			
		} else if(!regexp.test(value) && value.length) {

			validAlert(obj);
			$('.acceptblock').show();
			$('.acceptblock').html('<h2>Consent to Processing of Personal Data</h2>' +
			'<p>This Consent to Processing of Personal Data (hereinafter referred to as ‘the Consent’) governs relations between Alliance Media Strategy LLC (hereinafter referred to ‘the Operator’) and the natural person ' + $('#surname').val() + ' ' + $('#name').val() + ' (hereinafter referred to as ‘the User’) in terms of submission and processing of the User’s personal data.</p>'+
			'<p>Processing of personal data is performed in accordance with Federal Law 152-FZ dd 27.07.2006 on Personal Data (hereinafter referred to as ‘the Law’).</p>'+
			'<p>1. The User submits his or her personal data on the website www.gaidarforum.ru through filling the online application, and the Operator processes the User’s personal data both with automation facilities and without such.</p>'+
			'<p>2. The User makes his or her decision to submit his or her personal data and consents to their processing willfully and in his or her interests except as stipulated in article 9 part 2 of the Law.'+
			'<p>3. The User shall bear responsibility for submission of personal data of other persons.</p>'+
			'<p>4. The Consent shall become valid when the User agrees herewith on the website www.gaidarforum.ru through checking the field ‘I agree’.</p>'+
			'<p>5. The purpose of processing the submitted personal data is registration of a Forum participant.</p>'+
			'<p>6. The User agrees that the Operator is entitled to store and process, including automatic processing, any information related to the User’s personal data in accordance with the Law, including gathering, systematization, accumulation, storing, specification, distribution (including transmit thereof), anonymization, destruction of the personal data submitted by the User.</p>'+
			'<p>7. The Consent may be withdrawn by the Operator in his or her personal profile on www.gaidarforum.ru</p>'+
			'<p>8. Personal data, processing of which the User consents to, are as follows: family name, first name, patronymic; date of birth; place of birth, address; citizenship; photograph; sex; contact phone number; model of vehicle; its plate number; family name, first name and patronymic of driver; contact person; e-mail address; passport data, namely, document type; document series and number; issuing authority; its designation and code; iv) date of issue.</p>'+
			'<p>9. The User hereby confirms that his or her submitted data belong to him or her.</p>'+
			'<p>10. The User confirms and agrees that he or she has read this Consent and the conditions of the Operator’s processing of the User’s personal data submitted in the online application with care and in full measure; the text of this consent as well as the conditions of processing of the User’s personal data are clear to him or her.</p>');
			
		} else {
			alertClear(obj);
		}
		
	});
	
	/*===============================================================================*/
	
	/*==================При фокусе на #surname, #name, #patronymic подсветка подсказки по транслитерации===================*/
	
	$('body').on('focus', '#surname, #name, #patronymic', function(){
		var id = $(this).attr('id');
		var value  = $(this).val();
		var translval = $('#latin-' + id);
		 
		if(translval.hasClass('alert-text-input')) {
			$(translval).siblings('.alert-msg-block').css({'display' : 'block'});
		}
	});
	
	

	
	/*===============================================================================*/
	
	/*==========Изменение регистра букв #surname, #name, #patronymic=================*/
	
	$('body').on('keypress', '#surname, #name, #patronymic', function(){
		
		var val = '';
		var id = $(this).attr('id');
		var value  = $(this).val();
		var translval = $('#latin-' + id);
		var regexp = / /;
		var regexp2 = /\-/;
		
		if(regexp.test(value)){
		
		var splitValue = value.split(' ');
			for(i = 0; i <= (splitValue.length - 1); i++) {
				if(i !== 0) {
					val += ' ' + splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
				} else {
					val += splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
				}
			}
		} else if(regexp2.test(value)) {
		
			var splitValue = value.split('-');
			for(i = 0; i <= (splitValue.length - 1); i++) {
				if(i !== 0) {
					val += '-' + splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
				} else {
					val += splitValue[i].charAt(0).toUpperCase() + splitValue[i].substr(1).toLowerCase();
				}
			}
		} else {
			val = value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
		}
		
		$(this).val(val);
		

		
	});
	
	
	/*===============================================================================*/
	
	
	/*===============================================================================*/
	
	
	/*=====Проверка на введенные буквы русского алфавита  #organization, #position====*/
	
	$('body').on('keyup','#organization, #position', function() {
		var regexp = /[^a-zа-я -]/i;
		var text = 'Use the letters of the English alphabet';
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: text
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
	});
	
	/*===============================================================================*/
	
	
	
	/*===========================Проверка отмеченого radio===========================*/

	$('body').on('click', '.jq-radio', function(){
		
		if($(this).hasClass(".checked")){
			$(this).parent('.row').removeClass('error-radio-input');
			$(this).parent('.row').addClass('valid-radio-input');
		}
	});
	
	/*===============================================================================*/
	
	/*===================Получение списка городов, при выборе области=====================*/
	
			$(document).on('click', '.region .jq-selectbox__dropdown li.selected', function(){
				
				val = $(this).html();
					
				$('select[name="user_f_108"]').html('');
				$('.city').find('.error-msg-block').remove();

				$.post(
					'auth/select_city.php', {
					"select_val":  val
					}, function(data) {
						$('select[name="user_f_108"]').append('<option></option>' + data);
						
						setTimeout(function() {  
							  $('select[name="user_f_108"]').trigger('refresh');  
							}, 1);
					})
		
			});
	
		// $(document).on('click', 'select[name="user_f_107"] option', function(){
			// console.log(111);
			// val = $(this).val();
			
			// $('.city').find('.error-msg-block').remove();
			
			// $('select[name="user_f_108"]').html('<option></option>');

			// $.post(
				// 'auth/select_city.php', {
				// "select_val":  val
				// }, function(data) {
					// $('select[name="user_f_108"]').append(data);	
				// })
		// });
		
	/*===============================================================================*/
			$('body').on('keyup', '#user_f_106, #user_f_107, #user_f_108, #user_f_172', function() {
					var value = $(this).val();
			
			var regexp = /[^а-яёa-z .-]/i;
			var value  = $(this).val();
			
			
			if(regexp.test(value) && $(this).val().length) {
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Use the letters of the English alphabet'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
				
			} else {
				$(this).removeClass('valid-text-input');
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
			
		});
	
	/*===================Проверка на корректные символы #issued=====================*/
	$('body').on('keyup', '#issued', function() {
		var value = $(this).val();
		
		var regexp = /[^а-яёa-z -\.№0-9]/i;
		var value  = $(this).val();
		
		
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Use the letters of the English alphabet'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
		
	});
	
	/*===============================================================================*/
	
	/*===================Проверка на корректные символы #pbirth=====================*/
	$('body').on('keyup', '#pbirth', function() {
		var value = $(this).val();
		var regexp = /[^а-яёa-z -\.№0-9]/i;
		
		
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Use the letters of the English alphabet'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
		
	});
	
	/*===============================================================================*/
	
	/*=========================Проверка #passportN==============================*/

	$('body').on('keyup', '#passportN', function(){
		var regexp = /[^a-z0-9]/i;
		var value  = $(this).val();
		
		
			
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.alert-msg-block').remove();
			$(this).siblings('.error-msg-block').remove();
			$(this).removeClass('alert-text-input');
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Use only numbers and letters of the English alphabet'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
		} else if($(this).val().length && !$(this).hasClass('error-text-input')) {
			$(this).removeClass('error-text-input');
			$(this).removeClass('valid-text-input');
			$(this).addClass('alert-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).siblings('.alert-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'alert-msg-block',
						html: 'Please enter your passport details. Be advised that details of any other documents certifying your right of residence in the Russian Federation are not valid for this registration.<br><hr>'+
							 'Введите, пожалуйста,  данные вашего паспорта. Данные любых других документов будут считаться некорректными.'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			} else {
		$(this).siblings('.alert-msg-block').remove();
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).removeClass('alert-text-input');
			$(this).siblings('.error-msg-block').remove();
		
		}
	});
	
	
	/*===============================================================================*/
	$('#fiodr, #contact').each( function(index) {
		
		var regexp = /[^а-яёa-z ]/i;
		var value  = $(this).val();

		if(regexp.test(value) && $(this).val().length) {
			event.preventDefault();
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Use the letters of the English alphabet'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
	});
	
	/*=========================Проверка #code========================================*/
	
	$('body').on('keyup', '#code', function(){
	
		var regexp = /[0-9]{3}-[0-9]{3}/;
		var regexp2 = /^[0-9]-/
		var defvar = /___-___/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			
		} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Введите 6 цифр кода подразделения по формату "XXX-XXX"'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
		
		}
	
	});
	
	/*===============================================================================*/
	
	
	// Проверка на корректность e-mail адреса
	$('body').on('keyup', 'input[type=text]#email', function(){
		
		var regexp = /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/;
		var value  = $(this).val();
		
		if(regexp.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
			
		} else if(!regexp.test(value) && $(this).val().length) {
		$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Enter e-mail in the format: email@domain.ru'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
		
	});
	
	
	
	/* 	// Проверка введенной даты при клике
	$(document).on('click', 'td', function(){
	
		var regexp = /(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d/;
		var defvar = /__.__.____/;
		var value  = $(#date-b').val();
		
		console.log(value);
		
		if(regexp.test(value) && $('#date-b').val().length && !defvar.test(value)) {
			$('#date-b').removeClass('error-text-input');
			$('#date-b').addClass('valid-text-input');
			
		} else if(!regexp.test(value) && $('#date-b').val().length && !defvar.test(value)) {
			$('#date-b').removeClass('valid-text-input');
			$('#date-b').addClass('error-text-input');
			
		} else {
			$('#date-b').removeClass('valid-text-input');
			$('#date-b').removeClass('error-text-input');
		
		}
	
	}); */
	$('#contact, #fiodr').keyup( function() {
		var regexp = /[^a-zа-яё ]/i;
		var value  = $(this).val();
		
		
		if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Use the letters of the English alphabet'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
		} else if(!regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		}
	});
	
	// Проверка введенной даты
	$('body').on('keyup', '#date, #date-b', function(){
	
		var regexp = /(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d/;
		var defvar = /__.__.____/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).addClass('valid-text-input');
			
		} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).after($('<div/>', {
				class: 'error-msg-block',
				text: 'Enter the date in the format "dd.mm.yy"'
			}).append('<div/>'));
			$('this').parent('.row').css({'position' : 'relative'});
			
		} else {
			$(this).siblings('.error-msg-block').remove();
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
		
		}
	
	});
	
	// Проверка введенной даты
	$('body').on('change', '#date, #date-b', function(){
	
		var regexp = /(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d/;
		var defvar = /__.__.____/;
		var value  = $(this).val();
		
		if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).addClass('valid-text-input');
			
		} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).after($('<div/>', {
				class: 'error-msg-block',
				text: 'Enter the date in the format "dd.mm.yy"'
			}).append('<div/>'));
			$('this').parent('.row').css({'position' : 'relative'});
			
		} else {
			$(this).removeClass('valid-text-input');
			$(this).removeClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
		
		}
	
	});
		$('.jq-file__name').change( function(){
		if($(this).text() == 'Файл не выбран') {
			$('.file').addClass('error-text-input');
			$('.file').append($('<div/>', {
						class: 'error-msg-block',
						text: 'Select an image to upload'
					}).append('<div/>'));
			$('.file').css({'position' : 'relative'});
		} else {
			$('.file').removeClass('error-text-input');
			$('.file').children('.error-msg-block').remove();
		}
		
	});	
		
	
		$(document).on('keyup', '#mob', function() {
			var regexp = /((8|\+7)[\- ]?)?(\(?\d{3,5}\)?[\- ]?)?[\d\- ]{7,10}$/;
			var value  = $(this).val();
			
			
				if(regexp.test(value) && $(this).val().length) {
				$(this).removeClass('error-text-input');
					$(this).addClass('valid-text-input');
					$(this).siblings('.error-msg-block').remove();
					
					
				} else if(!regexp.test(value) && $(this).val().length) {
					$(this).removeClass('valid-text-input');
					$(this).addClass('error-text-input');
					$(this).siblings('.error-msg-block').remove();
					$(this).after($('<div/>', {
								class: 'error-msg-block',
								html: 'Enter the phone number in the format: <br /> +7 (495) 900 0000'
							}).append('<div/>'));
					$(this).parent('.row').css({'position' : 'relative'});
					
				} else {
					$(this).removeClass('valid-text-input');
					$(this).removeClass('error-text-input');
					$(this).siblings('.error-msg-block').remove();
				}
			
		});
		$('#marka, #gos, #transport, #photoapp, #videoapp').keyup( function() {
			if($(this).val().length) {
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).addClass('valid-text-input');
			} else {
				$(this).removeClass('valid-text-input');
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
		});
				

	
	
	// Проверка значений формы перед отправкой
	$('body').on('click', 'input[type=submit]', function (event) {
		if($('div').is('.error-msg-box')) {
			$('.error-msg-box').remove();
		}
		
		$('.row input[type=text]:not(#marka, #gos, #fiodr)').each( function(index){
			
			if(!$(this).val().length) {
				event.preventDefault();
				$(this).addClass('error-text-input');
				
				$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'The field can not be empty!'
				}).append('<div/>'));
					
				$(this).parent('.row').css({'position' : 'relative'});
			} else {
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
			
			
		
		});
		
		if(!$('#autochk-styler').hasClass('checked')){
			$('#marka, #gos, #fiodr').each( function(index){
				if(!$(this).val().length) {
					event.preventDefault();
					$(this).addClass('error-text-input');
					
					$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'The field can not be empty!'
					}).append('<div/>'));
						
					$(this).parent('.row').css({'position' : 'relative'});
				} else {
					$(this).removeClass('error-text-input');
					$(this).siblings('.error-msg-block').remove();
				}
			
			});
		} else {
			$('#marka, #gos, #fiodr').each( function(index){
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			});
		}
		
		$('.jq-file__name').each( function(index){
			if($(this).text() == 'Файл не выбран') {
				event.preventDefault();
				$('.file').addClass('error-text');
				$('.file').append($('<div/>', {
							class: 'error-msg-block',
							text: 'Select an image to upload'
						}).append('<div/>'));
				$('.file').css({'position' : 'relative'});
				
			} else {
				$('.file').removeClass('error-text');
				$('.file').children('.error-msg-block').remove();
			}
			
		});	
		
		
		/*if($('.row input').hasClass('error-text-input')) {
				event.preventDefault();
				var scrollTop = $('.error-text-input').offset().top - 50;
				$(document).scrollTop(scrollTop);
				$(this).siblings('.error-msg-block').css({'display' : 'none'});
			}
		});*/
			
		/*$('.row .jq-selectbox__select-text').each( function(index) {
			if($(this).text() === 'Выберите...') {
				event.preventDefault();
				$(this).addClass('error-text-input');
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Выберите значение из списка'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
			} else {
				
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
		});*/
		
		/*$('.row #date, #date-b').each( function(index){
	
			var regexp = /(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d/;
			var defvar = /__.__.____/;
			var value  = $(this).val();
			
			if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				
			} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
				
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				
			} 
	
		});*/
		
		
		/*$('.row .jq-file__name').each( function(index){
			if($(this).html() === 'Файл не выбран') {
				event.preventDefault();
				$('.file').addClass('error-text-input');
				$('.file').append($('<div/>', {
							class: 'error-msg-block',
							text: 'Выберите изображение для загрузки'
						}).append('<div/>'));
				$('.file').css({'position' : 'relative'});
			} else {
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
			
		});*/
		
	/*	$('.row #passportS').each( function(index){
	
			var regexp = /[0-9]{2} [0-9]{2}/;
			var defvar = /__ __/;
			var value  = $(this).val();
			
			if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
				
			} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Введите 4 цифры серии паспорта по формату "XX XX"'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
			} 
	
		});
		
		$('.row #code').each( function(index){
	
			var regexp = /[0-9]{3}-[0-9]{3}/;
			var defvar = /___-___/;
			var value  = $(this).val();
			
			if(regexp.test(value) && $(this).val().length && !defvar.test(value)) {
			
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				
			} else if(!regexp.test(value) && $(this).val().length && !defvar.test(value)) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				
			}
	
		});
		
		$('.row  #latin-surname, #latin-name').each( function(index){
			var regexp = /[^a-zA-Z-]/;
			var value  = $(this).val();
			var value = value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
			
			$(this).val(value);

			if(regexp.test(value) && $(this).val().length) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Используйте буквы английского алфавита'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
				
			}
		});
		
			$('.row  #latin-organization, #latin-position').each( function(index){
			var regexp = /[^a-zA-Z- ]/;
			var value  = $(this).val();
			
			
			$(this).val(value);

			if(regexp.test(value) && $(this).val().length) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Используйте буквы английского алфавита'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
				
			}
		});
		
		$('.row input[type=text]:not(#passportN, #passportS, #date, #code,'+ 
												'#date-b, #phone, #email,'+
												'#latin-surname, #latin-name, #organization, #position, #latin-organization, #latin-position, #marka, #gos)').each( function(index) {
			var regexp = /[^а-яА-ЯёЁ .]/;
			var value  = $(this).val();
			
			$(this).val(value);

			if(regexp.test(value) && $(this).val().length) {
				event.preventDefault();
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Используйте буквы русского алфавита'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				$(this).siblings('.error-msg-block').css({'display' : 'none'});
				
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('error-text-input');
				$(this).addClass('valid-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
		});
		
		
		$('.row input[type=text]#email').each( function(index){
		
		var regexp = /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/;
		var value  = $(this).val();
		
		if(regexp.test(value)) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
			
		} else if(!regexp.test(value) && $(this).val().length) {
		$(this).removeClass('valid-text-input');
			$(this).addClass('error-text-input');
			$(this).siblings('.error-msg-block').remove();
			$(this).after($('<div/>', {
						class: 'error-msg-block',
						text: 'Введите e-mail по формату: email@domain.ru'
					}).append('<div/>'));
			$(this).parent('.row').css({'position' : 'relative'});
			
			
		} 
		
		$('#phone').each( function(index) {
			var regexp = /[0-9]/;
			var value  = $(this).val();
			
			if(regexp.test(value) && $(this).val().length) {
			$(this).removeClass('error-text-input');
			$(this).addClass('valid-text-input');
			$(this).siblings('.error-msg-block').remove();
			
			} else if(!regexp.test(value) && $(this).val().length) {
				$(this).removeClass('valid-text-input');
				$(this).addClass('error-text-input');
				
				$(this).siblings('.error-msg-block').remove();
				$(this).after($('<div/>', {
							class: 'error-msg-block',
							text: 'Используйте только цифры'
						}).append('<div/>'));
				$(this).parent('.row').css({'position' : 'relative'});
				
			}
		});*/
		
		
		
		
	
		if($('.row input').is('.error-text-input')) {
			event.preventDefault();
			$(this).siblings('.error-msg-block').css({'display' : 'none'});
			(this).siblings('.alert-msg-block').css({'display' : 'none'});
			
			$('.content-block form').prepend($('<div>', {
				class: 'error-msg-box',
				html: '<span>Failed to send the form</span><br />Fix incorrectly filled fields marked with a red cross, following the prompts below field'
			}));
			
			var scrollTop = $('.error-msg-box').offset().top - 10;
			$(document).scrollTop(scrollTop);
		}
		
	});	
	
		//При потери фокуса отправляем данные на проверку
		/*$('body').on('blur', 'input:not(.jq-selectbox__search input)', function() {
			var field = $(this).attr('name');
			var value = $(this).val();
			var defval = ['', '__ __', '______', '___-___'];
			
			if(($.inArray(value, defval) === -1) && !$(this).hasClass('error-text-input')){
				sendAjax(field, value);
				
			} else if($.inArray(value, defval) === -1) {
				$(this).removeClass('valid-text-input');
				$(this).removeClass('error-text-input');
				$(this).siblings('.error-msg-block').remove();
			}
		});
		
		
		
		$(document).on('click', '.city .jq-selectbox', function(){
		
			if($(this).find('li').text() === '') {
				
				$('.city .jq-selectbox').after($('<div/>', {
							class: 'error-msg-block',
							text: 'Сначала выберите регион'
						}).append('<div/>'));
			}
		});*/
		
		$(document).on('focus', '.country .jq-selectbox', function(){
				
			if(!$('select[name="user_f_106"] option').length) {
			
				$.getJSON('list/country.php', {
				}).done(function(data) {
					 $.each( data, function( i, item ) {
						console.log(i);
					 });
				   });
				
			}
		});

	});
	

})(jQuery)