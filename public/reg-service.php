<?php //require_once('php/select_city_new.php'); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Регистрация персонала на Гайдаровский форум 2016</title>
    <meta charset="utf-8" />

    <meta name="description" content="Гайдаровский форум, Россия и мир: устойчивое развитие" />

    <link rel="apple-touch-icon" href="/touchicon.png">

    <link rel="icon" href="/favicon.png">

    <!--[if IE]>
    <link rel="shortcut icon" href="/favicon.ico">
    <![endif]-->

    <!-- IE10 -->
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="tileicon.png">

    <link href="css/style.css" rel="stylesheet" />
    <link media="print" rel="stylesheet" href="css/print.css">
    <link href='http://fonts.googleapis.com/css?family=Jura:400,600,500,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="css/lightbox.css" rel="stylesheet" />

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="js/lightbox-2.6.min.js"></script>
    <!--[if lt IE 9]>
    <script src="/js/html5/html5shiv.js"></script>
    <![endif]-->


    <style>
        .ranepa {
            margin-left: 158px;
        }

        .active-m {
            background: #951a1d; /* Old browsers */
            /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzk1MWExZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM3MzExMTQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
            background: -moz-linear-gradient(top,  #951a1d 0%, #731114 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#951a1d), color-stop(100%,#731114)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  #951a1d 0%,#731114 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  #951a1d 0%,#731114 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  #951a1d 0%,#731114 100%); /* IE10+ */
            background: linear-gradient(to bottom,  #951a1d 0%,#731114 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#951a1d', endColorstr='#731114',GradientType=0 ); /* IE6-8 */
            padding: 5px 2px;
            border-radius: 4px 4px 0px 0px;
        }

        .menu-content {
            background: none;
            border: 0px;
            border-bottom: 3px solid #AA1E2B;
            list-style-type: none;
            margin-bottom: 15px;
            margin-top: -5px;
            overflow: hidden;
        }
        .lang {
            position: relative;
            right: 0px;
            top: 15px;
        }
        .term-block h4 {
            font-family: jura;
            font-size: 18px;
            text-align: center;
            font-weight: bold;
        }
        .day-date {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #AD1D2B;
            color: #AD1D2B;
            font-size: 18px;
            margin-top: -4px;
            padding: 2px 0;
        }
        .enter {
            color: #777777;
            float: right;
            left: -79px;
            position: relative;
            top: 17px;
            width: 115px;
        }
        .enter > a {
            color: #777777;
        }
        .day-number {
            background-color: #AD1D2B;
            color: #FFFFFF;
            font-family: jura;
            font-size: 18px;
            margin: 1px 0;
        }

        .term-day-name {
            color: #666666;
            font-family: jura;
            font-size: 15px;
            margin: 2px 0;
        }
        .day-wrapper {
            float: left;
            height: 96px;
            margin: 10px 10px 0 0;
            padding: 0;
            text-align: center;
            width: 197px;
        }
        .logo-block > p {
            font-family: jura;
            font-size: 17px;
            margin: 5px;
        }
        .searchbutton {
            background: url("images/search-icon.png") no-repeat;
            border: 0 none;
            cursor: pointer;
            width: 16px;
            height: 15px;
        }
        .twitter {
            background: url("images/tw.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
            display: block;
            float: right;
            height: 26px;
            position: relative;
            right: 12px;
            top: 8px;
            width: 19px;
        }

        #search-form {
            float: right;
            position: relative;
            right: 26px;
            top: 7px;
        }

        .main-menu {
            float: left;
            font-size: 19px;
            list-style-type: none;
            margin-bottom: 18px;
            margin-left: 0.2em;
            overflow: hidden;
            padding-left: 25px;
        }

        .registration-button {
            background: url("images/registration-button.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
            display: block;
            height: 67px;
            margin-left: 15px;
            overflow: hidden;
            text-indent: 100%;
            white-space: nowrap;
            width: 229px;
        }

        .logo-block {
            clear: both;
            margin-left: 20px;
            margin-top: 42px;
            text-align: left;
        }

        .leftside {
            width: 300px;
        }

        #seach_id {
            border: 1px solid #E5E5E5;
            border-radius: 4px;
            color: #999999;
            font-size: 12px;
            padding: 0px 2px;
            width: 120px;
            height: 20px;
        }

        .extented-block-small {
            background-color: #F7F7F7;
            border-bottom: 0.1em solid #E5E5E5;
            border-left: 0.1em solid #E5E5E5;
            border-top: 0.1em solid #E5E5E5;
            float: left;
            height: 34px;
            margin-top: 9px;
            width: 978px;
            border-right: 0.1em solid #e5e5e5;
        }

        .logo-block img {
            margin: 10px 5px 35px;
        }

        .day-link > p {
            margin: 10px 0;
        }
        .day-link {
            color: #444444;
            line-height: 1.3;
            padding: 0 12px;
            position: relative;
            top: 0px;
        }
        .left-wrapper {
            background: none repeat scroll 0 0 #F7F7F7;
            border: 1px solid #E5E5E5;
            float: right;
            margin-bottom: 10px;
            min-height: 640px;
            width: 647px;
        } .reg:hover {
              color: #fff;
          }
        .reg {
            background: none repeat scroll 0 0 #951a1d;
            border-radius: 8px;
            color: #ffffff;
            float: left;
            font-size: 16px;
            margin: 2px 0 0 24px;
            padding: 7px 25px;
            text-decoration: none;
        }
        .enter_n{
            background-color: #fff;
            border-radius: 4px;
            color: #777777;
            float: right;
            left: 0;
            margin-top: 12px;
            padding: 5px;
        }
    </style>

    
    <!--script>
        (function($) {
            $(document).ready( function() {
                var pathname = '/registration/data.php';
                
                $.get( pathname, { reg: 1}).done( function( data ) {
                    var content = $( data ).find( ".content-block" ).html();
                    $( ".content-block" ).empty().append( content );
                });
            });
        })(jQuery);
        
    </script--></head>
<body>
<div class="wrapper">
    <div class="header-wrapper">
        <header>
            <a href="/ru/2015/"><div class="logo"></div></a>

            <div class="lang"><a href="/ru/2015" class="ru">Russian version</a> <a href="/en/2015" class="en">English verision</a></div>
            <!--<div class="enter_n"><a href="/ru/login">Войти</a> | <a href="/registration">Регистрация</a></div> -->
        </header>
    </div>
    <div class="content-wrapper">
        <div class="extented-block-small">
            <!--sb_index_start--><style>
            .extented-block-small {
                position: relative;
            }
        </style>
            <div class="year"><a href="/"><b>2016</b></a></div>
            <div class="year"><a href="/ru/2015">2015</a></div>
            <div class="year"><a href="/ru/2014">2014</a></div>
            <div class="year"><a href="/ru/2013">2013</a></div>
            <div class="year"><a href="/ru/2012">2012</a></div>
            <div class="year"><a href="/ru/2011">2011</a></div>
            <!-- <a class="reg" href="/registration/">Регистрация</a> --><!--sb_index_end-->
            <a class="twitter" href="https://www.facebook.com/GaidarForum"></a>
            <div id="search-form">
                <form action='http://www.gaidarforum.ru/ru/search.php' method='get'>
                    <input id="seach_id" name="sb_search_words" class="textarea"  type="text" placeholder="Найти..." value="" />
                    <input  class="searchbutton" type="submit" value="" />
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="leftside">


        <?php require "inc/block-menu.html"; ?>

            <div class="logo-block">
                <style>
                    .logo-block > a {
                        display: block;
                    }
                    .logo-block > p{
                        font-size: 12px;
                        margin: 5px 10px;
                    }
                </style>
                <a href="http://www.ranepa.ru/"><img src="images/ranepa-logo.png" width="125" alt="" title="" /></a>
                <a href="http://www.iep.ru/"><img src="images/institute-gaidar-logo.png" width="130" alt="" title="" /></a>
            </div>

        </div>
        <!--div class="news-block-mini">
        <div class="news-block-title">
        <div class="rss-block">
        <a href="/ru/rss.php"><div class="rss"></div></a>
        <h1>Новости</h1>
    </div>
        <div class="archive-block">
        <a href="/ru/news.php"><h1>Все новости</h1></a>
        <div class="archive"></div>
    </div>
    </div>
        <div class="clearfix"></div>
        
        <div class="clearfix"></div>
    </div-->
        <div class="content-inner">
            <div class="content-block">


                <!--sb_index_start-->


                <link href="css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

                <link href="css/jquery.formstyler.css" rel="stylesheet" />
                <link href="css/cropper.css" rel="stylesheet" />

                <style>
                    .nocity {
                        clear: both;
                        margin: 13px;
                        position: relative;
                    }

                    #man-block {
                        display: none;
                    }

                    .acceptblock {
                        background-color: #FFFFFF;
                        border: 1px solid #CCCCCC;
                        height: 200px;
                        margin: 10px 70px;
                        overflow-y: scroll;
                    }


                </style>


<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <?php require "inc/form-service.html"; ?>

<!-- +++++++++++++++++++++++++++++++++++++++++++++++ -->


                <!--div class="overlays"></div>
                <div class="modal-change">
                <p>Укажите ваше гражданство:</p>
                <a href="" >Российская федерация</a>
                <a href="" ></a>
                </div-->

                <script src="js/jquery.min.js"></script>
                <script src="js/jquery-ui-1.10.4.custom.min.js"></script>
                <script src="js/jquery.ui.datepicker-ru.js"></script>
                <script src="js/jquery.placeholder.min.js"></script>
                <script src="js/jquery.formstyler.min.js"></script>
                <script src="js/jquery.maskedinput.min.js"></script>
                <script src="js/jquery.validateForm.js"></script>
                <script src="js/jquery.modalWindow.js"></script>


                <script>
                    (function($) {
                        $("#passportS").mask("99 99");
                        $("#passportN").mask("999999");
                        $("#code").mask("999-999");
                        $("#phone").mask("+7 (999) 999-9999");
                        $("#date, #date-b").mask("99.99.9999");

                        $("#date").datepicker({
                         changeMonth: true,
                         changeYear: true,
                         yearRange: "1914:2002",
                         });

                         $("#date-b").datepicker({
                         changeMonth: true,
                         changeYear: true,
                         yearRange: "1914:2002",
                         });


                        $(document).ready( function() {
                            $('input, select').styler({ selectSearch: true });
                        });
                    })(jQuery);

                </script>
                <!--sb_index_end-->

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="partners-wrapper">
        <script>
            /*   ( function($) {        
             $(document).ready( function(){
             var elm = $('.partners-tabs li');
             $(elm).eq(0).addClass('active-tab');
             $('.p-block').eq(0).addClass('active-p');

             $(elm).click( function(){
             var index = $(this).index();
             console.log(index);
             $(this).addClass('active-tab').siblings('li').removeClass('active-tab');

             $('.p-block').eq(index).show().siblings('.p-block').hide();
             var off = $( ".footer-wrapper").offset().top;
             $( ".footer-wrapper" ).scrollTop(off);
             });
             } );
             })(jQuery)*/
        </script>

        <style>
            .p-block {
                float: left;
                margin: 15px 5px 0 0;
                width: 304px;
            }
            .p2598 .p-img > a, .p2599 .p-img > a, p2552 .p-img > a, p2551 .p-img > a{
                text-align: left;
            }

            .gen-info-p-block > div .p-img > a {
                display: table-cell;
                height: 100px;
                text-align: left;
                vertical-align: middle;
                width: 150px;
            }

            .p-img > a {
                display: table-cell;
                height: 100px;
                text-align: center;
                vertical-align: middle;
                width: 150px;
            }

            .p-img {
                display: inline-block;
            }
            .gen-info-p-block > div .p-img > a {
                display: table-cell;
                height: 100px;
                vertical-align: middle;
                width: 150px;
            }
            .partners-tabs > li {
                background-color: #EEEEEE;
                border: 1px solid #E3E3E3;
                border-radius: 2px 2px 0 0;
                color: #666666;
                cursor: pointer;
                float: left;
                font-size: 14px;
                margin: 5px 10px 0 0;
                padding: 5px 15px;
            }

            .gen-info-p-block {
                float: left;
                margin: 15px 0 0 30px;
                width: 149px;
            }
            .gen-info-p-block > h2:nth-child(3) {
                margin: 1px 0px 5px;
            }
            .partners-tabs {
                border-bottom: 1px solid #C4C4C4;
                margin: 5px 0 0;
                overflow: hidden;
            }

            .partners-tabs .active-tab {
                background-color: #C4C4C4;
                border: 1px solid #BBBBBB;
                color: #F4F4F4;
            }
            .partners-block{ height: auto; }

            .partners-block h2 {
                border-bottom: 1px solid #666666;
                clear: both;
                color: #666666;
                font-size: 14px;
                margin-bottom: 5px;
                padding-bottom: 5px;
                width: auto;
                margin-top: 15px;
            }

            .info-p-block {
                float: left;
                margin: 15px 0 0 35px;
                width: 453px;
            }
        </style>


    <?php require "inc/block-partners.html"; ?>

    </div>

    <div class="footer-wrapper">
        <footer>
            <div class="copyright">© 2010-2015, Гайдаровский форум</div>
            <div class="copyright-info">При использовании официальной информации и цитировании высказываний участников Форума <br /> готовые материалы должны содержать упоминание Гайдаровского форума и источник полученной <br /> информации, а для электронных СМИ — содержать ссылку на официальный сайт Форума.</div>
            <div class="developer-info">Разработка и дизайн сайта: <br /> <a style="color: #fff" href="http://www.amska.ru" title="Коммуникационная группа АМС" target="_blank">Коммуникационная группа АМС</a> в составе ГК АКИГ</div>
        </footer>
    </div>
</div>


<!--Openstat-->

<span id="openstat2290247"></span>

<script type="text/javascript">
    var openstat = { counter: 2290247, next: openstat };
    (function(d, t, p) {
        var j = d.createElement(t); j.async = true; j.type = "text/javascript";
        j.src = ("https:" == p ? "https:" : "http:") + "//openstat.net/cnt.js";
        var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);
    })(document, "script", document.location.protocol);
</script>

<!--/Openstat-->


<div class="ajax-login-block" id="modal">
    <h1>Вход в личный кабинет</h1>
    <div id="close"></div>
    <p><b>Для входа в личный кабинет, пожалуйста, авторизируйтесь.</b></p>
    <form class="enterForm" id="login" action="/ru/profile/" method="post">
        <input type="text" name="su_email_ajax" value="" placeholder="E-mail" />
        <input type="password" name="su_pass_ajax" placeholder="Пароль" />
        <input type="submit" value="Войти" id="submit" />
    </form>
    <p>Не знаете или забыли пароль? Воспользуйтесь <a href="/ru/login/recovery/">формой восстановления пароля</a> или <a href="/registration">зарегистрируйтесь</a>.</p>
</div>

      <!--script src="/js/2015/ajax-login-form.js"></script-->
</body>

</html>