
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Participant registration for the Gaidar Forum 2016</title>
    <meta charset="utf-8" />

    <meta name="description" content="Гайдаровский форум, Россия и мир: устойчивое развитие" />

    <link rel="apple-touch-icon" href="/touchicon.png">

    <link rel="icon" href="/favicon.png">

    <!--[if IE]>
    <link rel="shortcut icon" href="/favicon.ico">
    <![endif]-->

    <!-- IE10 -->
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="tileicon.png">

    <link href="css/style.css" rel="stylesheet" />
    <link media="print" rel="stylesheet" href="css/print.css">
    <link href='http://fonts.googleapis.com/css?family=Jura:400,600,500,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="css/lightbox.css" rel="stylesheet" />

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="js/lightbox-2.6.min.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5/html5shiv.js"></script>
    <![endif]-->


    <style>
        .ranepa {
            margin-left: 158px;
        }
        .lang {
            position: relative;
            right: 0px;
            top: 15px;
        }
        .reg:hover {
            color: #fff;
        }
        .reg {
            background: none repeat scroll 0 0 #951a1d;
            border-radius: 8px;
            color: #ffffff;
            float: left;
            font-size: 16px;
            margin: 2px 0 0 24px;
            padding: 7px 25px;
            text-decoration: none;
        }
        .term-block h4 {
            font-family: jura;
            font-size: 18px;
            text-align: center;
            font-weight: bold;
        }
        .day-date {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #AD1D2B;
            color: #AD1D2B;
            font-size: 18px;
            margin-top: -4px;
            padding: 2px 0;
        }
        .enter {
            color: #777777;
            float: right;
            left: -79px;
            position: relative;
            top: 17px;
        }
        .enter > a {
            color: #777777;
        }
        .day-number {
            background-color: #AD1D2B;
            color: #FFFFFF;
            font-family: jura;
            font-size: 18px;
            margin: 1px 0;
        }

        .term-day-name {
            color: #666666;
            font-family: jura;
            font-size: 15px;
            margin: 2px 0;
        }
        .day-wrapper {
            float: left;
            height: 96px;
            margin: 10px 10px 0 0;
            padding: 0;
            text-align: center;
            width: 197px;
        }
        .logo-block > p {
            font-family: jura;
            font-size: 17px;
            margin: 5px;
        }
        .searchbutton {
            background: url("images/search-icon.png") no-repeat scroll center center rgba(0, 0, 0, 0);
            border: 0 none;
            cursor: pointer;
            width: 16px;
        }
        .twitter {
            background: url("images/tw.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
            display: block;
            float: right;
            height: 26px;
            position: relative;
            right: 13px;
            top: 12px;
            width: 18px;
        }

        #search-form {
            float: right;
            position: relative;
            right: 26px;
            top: 6px;
        }

        .main-menu {
            float: left;
            font-size: 19px;
            list-style-type: none;
            margin-bottom: 18px;
            margin-left: 0.2em;
            overflow: hidden;
            padding-left: 25px;
        }

        .registration-button-en {
            background: url("images/registration-button-en.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0);
            display: block;
            height: 67px;
            margin-left: 15px;
            overflow: hidden;
            text-indent: 100%;
            white-space: nowrap;
            width: 229px;
        }

        .logo-block {
            clear: both;
            margin-left: 20px;
            margin-top: 42px;
            text-align: left;
        }

        .leftside {
            width: 300px;
        }

        #seach_id {
            border: 1px solid #E5E5E5;
            border-radius: 4px;
            color: #999999;
            font-size: 12px;
            padding: 0px 2px;
            width: 120px;
            height: 20px;
        }

        .extented-block-small {
            background-color: #F7F7F7;
            border-bottom: 0.1em solid #E5E5E5;
            border-left: 0.1em solid #E5E5E5;
            border-top: 0.1em solid #E5E5E5;
            float: left;
            height: 34px;
            margin-top: 9px;
            width: 980px;
            border-right: 0.1em solid #e5e5e5;
        }

        .logo-block img {
            margin: 10px 5px 35px;
        }

        .logo-en {
            background: url('images/logo2016en.png') no-repeat;
            width: 720px;
            height: 120px;
            position: relative;
            top: 30px;
            float: left;
        }

        .day-link > p {
            margin: 10px 0;
        }
        .day-link {
            color: #444444;
            line-height: 1.3;
            padding: 0 12px;
            position: relative;
            top: 0px;
        }
        .left-wrapper {
            background: none repeat scroll 0 0 #F7F7F7;
            border: 1px solid #E5E5E5;
            float: right;
            margin-bottom: 10px;
            min-height: 640px;
            width: 647px;
        }
        .enter_n{
            background-color: #fff;
            border-radius: 4px;
            color: #777777;
            float: right;
            left: 0;
            margin-top: 12px;
            padding: 5px;
        }
    </style>
    <style>
        .redd > a {
            background: linear-gradient(to bottom, #951A1D 0%, #731114 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
            border-radius: 8px;
            color: #FFFFFF;
            display: block;
            float: left;
            font-size: 13px;
            height: 45px;
            margin: 10px 18px;
            padding: 10px 20px;
            text-align: center;
            text-decoration: none;
            width: 297px;
        }
        .redd {
            background: none repeat scroll 0 0 #fff;
            border-radius: 8px;
            bottom: 0;
            z-index: 1000
        height: 200px;
            left: 0;
            margin: auto;
            position: absolute;
            right: 0;
            top: 0;
            width: 412px;
        }
    </style></head>
<body>
<div class="wrapper">
    <div class="header-wrapper">
        <header>
            <a href="/en/2015"><div class="logo-en"></div></a>

            <div class="lang"> <a href="/ru/2015" class="ru">Russian version</a><a href="/en/2015" class="en">English verision</a></div>
            <!--<div class="enter_n"><a href="/eng/login">Login</a> | <a href="/eng/registration">Registration</a></div> -->

        </header>
    </div>
    <div class="content-wrapper">
        <div class="extented-block-small">
            <style>
                .extented-block-small {
                    position: relative;
                }
            </style>
            <div class="year"><a href="/en/"><b>2016</b></a></div>
            <div class="year"><a href="/en/2015">2015</a></div>
            <div class="year"><a href="/en/2014">2014</a></div>
            <div class="year"><a href="/en/2013">2013</a></div>
            <div class="year"><a href="/en/2012">2012</a></div>
            <div class="year"><a href="/en/2011">2011</a></div>
            <!--a class="reg" href="/eng/registration/">Registration</a-->
            <a class="twitter" href="https://www.facebook.com/GaidarForum"></a>
            <div id="search-form">
                <form action='http://www.gaidarforum.ru/ru/search.php' method='get'>
                    <input id="seach_id" name="sb_search_words" class="textarea"  type="text" value="Search..." onblur="if(this.value==''){this.value='Search...';}"
                           onfocus="if(this.value=='Search...'){this.value='';}">
                    <input class="searchbutton" type="submit" value="">

                </form>
            </div>                </div>
        <div class="clearfix"></div>
        <div class="leftside">

        <?php require "inc/block-menu-en.html"; ?>

            <div class="logo-block">
                <style>
                    .logo-block > a {
                        display: block;
                    }
                    .logo-block > p{
                        font-size: 12px;
                        margin: 5px 10px;
                    }
                </style>
                <a href="http://www.ranepa.ru/"><img src="/images/ranepa-logo-en.png" width="125"  /></a>
                <a href="http://www.iep.ru/"><img src="/images/institute-gaidar-logo-en.png" width="115"  /></a>
                <!--p>With the support of</p>
                <a href="http://gaidarfund.ru/"><img src="/images/fond-gaidar-logo-en.png" width="105"  /></a-->

            </div>

        </div>

        <div class="content-inner">
            <div class="content-block">


                <!--sb_index_start--><link href="css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />

                <link href="css/jquery.formstyler.css" rel="stylesheet" />
                <link href="css/cropper.css" rel="stylesheet" />
                <style>
                    .text-photo {
                        color: #555555;
                        width: 255px;
                    }
                    .row.noauto {
                        border-radius: 4px;
                        color: #666666;
                        overflow: hidden;
                        padding: 2px 10px 0 0;
                        width: 225px;
                        margin: 0px 0px 25px 11px;
                    }
                    .nocity {
                        clear: both;
                        margin: 13px;
                        position: relative;
                    }
                    /*  .redd > a {
                    background: linear-gradient(to bottom, #951a1d 0%, #731114 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
                    border-radius: 8px;
                    color: #fff;
                    display: block;
                    float: left;
                    font-size: 13px;
                    margin: 68px 18px;
                    padding: 20px;
                    text-align: center;
                    text-decoration: none;
                    width: 130px;
                    }
                    .redd {
                    background: none repeat scroll 0 0 #fff;
                    border-radius: 8px;
                    bottom: 0;
                    height: 200px;
                    left: 0;
                    margin: auto;
                    position: absolute;
                    right: 0;
                    top: 0;
                    width: 412px;
                    }*/
                    #man-block {
                        display: none;
                    }

                    .acceptblock {
                        background-color: #FFFFFF;
                        border: 1px solid #CCCCCC;
                        height: 200px;
                        margin: 10px 70px;
                        overflow-y: scroll;
                    }
                </style>

                
<!-- ++++++++++++++++++++++++++++++++++++++ -->

                <?php require "inc/form-member-en.html"; ?>

<!-- ++++++++++++++++++++++++++++++++++++++++   -->

                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                <script src="js/jquery-ui-1.10.4.custom.min.js"></script>
                <script src="js/jquery.ui.datepicker-ru.js"></script>
                <script src="js/jquery.placeholder.min.js"></script>
                <script src="js/jquery.formstyler.min.js"></script>
                <script src="js/jquery.maskedinput.min.js"></script>
                <script src="js/jquery.validateForm-en.js"></script>
                <script src="js/jquery.modalWindow.js"></script>
                <script src="js/jquery.cookie.js"></script>


                <script>
                    (function($) {
                        /*  $("#passportS").mask("99 99");
                         $("#passportN").mask("999999");*/
                        $("#code").mask("999-999");
                        $("#phone").mask("(999) 999-9999");
                        $("#date, #date-b").mask("99.99.9999");

                        $("#date").datepicker({
                         changeMonth: true,
                         changeYear: true,
                         yearRange: "1914:2002",
                         });

                         $("#date-b").datepicker({
                         changeMonth: true,
                         changeYear: true,
                         yearRange: "1914:2002",
                         });

                        $(document).ready( function() {
                            $('body').append('<div class="redd" style="position: fixed;z-index: 112; width: 375px; height: 171px;"><a id="no" href="">Registration of foreign citizens<hr>Регистрация иностранных граждан</a><a id="yes" href="reg-member.php">Registration of Russian citizens<hr>Регистрация граждан Российской Федерации</a></div>');
                            $('body').append('<div class="overlays"></div>');

                            $(document).on('click', '#no', function(event){
                                event.preventDefault();
                                $('.redd').remove();
                                $('.overlays').remove();
                            });

                            $('input, select').styler({
                                selectSearch: true,
                                fileBrowse: 'Browse...',

                            });
                        });
                    })(jQuery);

                </script><!--sb_index_end-->

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="partners-wrapper">
        <script>
            /*   ( function($) {        
             $(document).ready( function(){
             var elm = $('.partners-tabs li');
             $(elm).eq(0).addClass('active-tab');
             $('.p-block').eq(0).addClass('active-p');

             $(elm).click( function(){
             var index = $(this).index();
             console.log(index);
             $(this).addClass('active-tab').siblings('li').removeClass('active-tab');

             $('.p-block').eq(index).show().siblings('.p-block').hide();
             var off = $( ".footer-wrapper").offset().top;
             $( ".footer-wrapper" ).scrollTop(off);
             });
             } );
             })(jQuery)*/
        </script>

        <style>
            .p-block {
                float: left;
                margin: 15px 5px 0 0;
                width: 304px;
            }
            .p2598 .p-img > a, .p2599 .p-img > a, p2552 .p-img > a, p2551 .p-img > a{
                text-align: left;
            }

            .gen-info-p-block > div .p-img > a {
                display: table-cell;
                height: 100px;
                text-align: left;
                vertical-align: middle;
                width: 150px;
            }

            .p-img > a {
                display: table-cell;
                height: 100px;
                text-align: center;
                vertical-align: middle;
                width: 150px;
            }

            .p-img {
                display: inline-block;
            }
            .gen-info-p-block > div .p-img > a {
                display: table-cell;
                height: 100px;
                vertical-align: middle;
                width: 150px;
            }
            .partners-tabs > li {
                background-color: #EEEEEE;
                border: 1px solid #E3E3E3;
                border-radius: 2px 2px 0 0;
                color: #666666;
                cursor: pointer;
                float: left;
                font-size: 14px;
                margin: 5px 10px 0 0;
                padding: 5px 15px;
            }

            .gen-info-p-block {
                float: left;
                margin: 15px 0 0 30px;
                width: 149px;
            }
            .gen-info-p-block > h2:nth-child(3) {
                margin: 1px 0px 5px;
            }
            .partners-tabs {
                border-bottom: 1px solid #C4C4C4;
                margin: 5px 0 0;
                overflow: hidden;
            }

            .partners-tabs .active-tab {
                background-color: #C4C4C4;
                border: 1px solid #BBBBBB;
                color: #F4F4F4;
            }
            .partners-block{ height: auto; }

            .partners-block h2 {
                border-bottom: 1px solid #666666;
                clear: both;
                color: #666666;
                font-size: 14px;
                margin-bottom: 5px;
                padding-bottom: 5px;
                width: auto;
                margin-top: 15px;
            }

            .info-p-block {
                float: left;
                margin: 15px 0 0 35px;
                width: 453px;
            }
        </style>

    <?php require "inc/block-partners-en.html"; ?>

</div>

    <div class="footer-wrapper">
        <footer>
            <div class="copyright">© 2010-2015, Gaidar Forum</div>
            <div class="copyright-info">When using the official information and quoting the statements of the Forum participants,<br> the materials shall contain a reference to the Gaidar Forum and the source of the information, <br>or, in case of electronic media - a reference to the official website of the Forum.</div>
            <div class="developer-info">This website was developed by: <br /><a  style="color: #fff;" href="http://www.amska.ru">“Alliance Media Strategy” LLC affiliated with the ACIG Group of Companies</a></div>
        </footer>
    </div>
</div>
<!--Openstat-->
<span id="openstat2290247"></span>
<script type="text/javascript">
    var openstat = { counter: 2290247, next: openstat };
    (function(d, t, p) {
        var j = d.createElement(t); j.async = true; j.type = "text/javascript";
        j.src = ("https:" == p ? "https:" : "http:") + "//openstat.net/cnt.js";
        var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);
    })(document, "script", document.location.protocol);
</script>
<!--/Openstat-->
</body>

</html>