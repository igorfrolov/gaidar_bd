/*
Navicat MySQL Data Transfer

Source Server         : vm-acig-2[gfadmin_db]
Source Server Version : 50546
Source Host           : 46.173.220.71:3306
Source Database       : gf_reg

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2015-12-11 17:51:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `sur_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `post` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('9', 'igor.frooff@yandex.ru', '$2y$10$n4PsncsqfVHx1rfdY1tMk.P/JPFW7Jexk1XJf6Qy6H5O5ZbrbqowW', '89647838416', 'Фролов', '', null, '1', null, null, '2015-11-20 19:35:06', '$2y$10$Mx5hmaHXjon9mvBCLJFVGOfbLJUVeg4LGna5h/QLMlMZNwuGldbS6', null, 'Игорь', null, '2015-04-28 12:26:53', '2015-11-20 19:35:06');
INSERT INTO `users` VALUES ('10', 'jenya_pupkin@yandex.ru', '$2y$10$tM9mFiFwE4bZx3Qvom7h3ucpEP/wcMSX1P51hL6izDdr2uIBC.epu', '999999999', 'Пупкин', '', null, '1', null, null, null, null, null, 'Женя', null, '2015-05-05 06:55:01', '2015-05-05 06:55:01');
INSERT INTO `users` VALUES ('11', 'pupok@ya.ru', '$2y$10$i0csJgy5HEBXf3szkMAS2ehGfHxOZEzmWlxty8GXZ2Qkf11l.GnbS', '1231241245124', 'sdakjfsdklf', '', null, '1', null, null, '2015-10-31 19:56:55', '$2y$10$L5ZxkeYDrEG8McW0sxtziuIry2/O8cXDTu2GVY/XWGwB2fxwgxvre', null, 'sdfj', null, '2015-10-31 19:56:18', '2015-10-31 19:56:55');
INSERT INTO `users` VALUES ('12', 'a.evseev@acig.ru', '$2y$10$8Fyhhi5qF/t8qhCGT7s9nO0NzMdX8e35ue/KIV3WgI.7frlJazSvO', '', '', '', null, '1', null, null, '2015-11-19 05:42:37', '$2y$10$HWU6kw1sS4CzruIjSt5IneCpfoDGPre5OAFRY8vkrxiZV2Ybbnlam', null, 'Алексей', null, '2015-11-18 08:56:02', '2015-11-19 05:42:37');
INSERT INTO `users` VALUES ('13', 'o.antonova@icloud.com', '$2y$10$5foULgWwEb1wJZaFZrla5uJ7kHm4DG77hzPq6sgnFs6/uuH7zCpv.', '', '', '', null, '1', null, null, '2015-11-18 19:45:56', '$2y$10$j8WciLJD3pCLGIDY0lX0veksbfPmBEAzlm7.WhHIhnKMPIApHfSO2', null, '', null, '2015-11-18 15:02:14', '2015-11-18 19:45:56');
INSERT INTO `users` VALUES ('14', 'mischenko@a5000.ru', '$2y$10$sn87WbhOlTiDbnqXJk.ALuNaXHIQIdOTOsIX1GvM44gXwufw0nYZi', '', '', '', null, '1', null, null, null, null, null, '', null, '2015-11-18 15:03:09', '2015-11-18 15:03:09');
INSERT INTO `users` VALUES ('15', 'npisarev@gmail.com', '$2y$10$rfcrtqHXD9vyTX21wero6uq1YZNEg1rxwrJd70Vb/hu9IDheCdVRa', '', '', '', null, '1', null, null, '2015-11-18 20:48:09', '$2y$10$GQNwhV9ePDQdeWxt0Wn.NO1Jq8Bfh8iVxZaUEfpuBO5eEID6KP5BO', null, '', null, '2015-11-18 20:47:37', '2015-11-18 20:48:09');
