/*
Navicat MySQL Data Transfer

Source Server         : vm-acig-2[gfadmin_db]
Source Server Version : 50546
Source Host           : 46.173.220.71:3306
Source Database       : gf_reg

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2015-12-11 17:51:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `status_id` tinyint(3) NOT NULL,
  `status_group` tinyint(3) DEFAULT NULL,
  `status_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of statuses
-- ----------------------------
INSERT INTO `statuses` VALUES ('1', '1', 'Данные внесены');
INSERT INTO `statuses` VALUES ('2', '1', 'Проверка данных');
INSERT INTO `statuses` VALUES ('3', '1', 'Требуются изменения');
INSERT INTO `statuses` VALUES ('4', '1', 'Внесены изменения');
INSERT INTO `statuses` VALUES ('5', '1', 'Данные проверены');
INSERT INTO `statuses` VALUES ('6', '2', 'Не проверен');
INSERT INTO `statuses` VALUES ('7', '2', 'На проверке');
INSERT INTO `statuses` VALUES ('8', '2', 'Проверен');
INSERT INTO `statuses` VALUES ('9', '2', 'Отведен');
INSERT INTO `statuses` VALUES ('10', '2', 'Не требуется');
INSERT INTO `statuses` VALUES ('11', '2', 'Требуются изменения');
INSERT INTO `statuses` VALUES ('12', '3', 'Новый');
INSERT INTO `statuses` VALUES ('13', '3', 'Утвержден');
INSERT INTO `statuses` VALUES ('14', '3', 'Отказан');
INSERT INTO `statuses` VALUES ('15', '3', 'Отложен');
