/*
Navicat MySQL Data Transfer

Source Server         : vm-acig-2[gfadmin_db]
Source Server Version : 50546
Source Host           : 46.173.220.71:3306
Source Database       : gf_reg

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2015-12-11 17:50:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bages
-- ----------------------------
DROP TABLE IF EXISTS `bages`;
CREATE TABLE `bages` (
  `bage_id` tinyint(3) NOT NULL,
  `bage_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bages
-- ----------------------------
INSERT INTO `bages` VALUES ('1', 'Partisipant');
INSERT INTO `bages` VALUES ('2', 'Partisipant+');
INSERT INTO `bages` VALUES ('3', 'Ranepa');
INSERT INTO `bages` VALUES ('4', 'Media');
INSERT INTO `bages` VALUES ('5', 'Organizer');
INSERT INTO `bages` VALUES ('6', 'Staff');
INSERT INTO `bages` VALUES ('7', 'Staff+');
INSERT INTO `bages` VALUES ('8', 'Volunteer');
INSERT INTO `bages` VALUES ('9', 'Volunteer+');
INSERT INTO `bages` VALUES ('10', 'Security');
INSERT INTO `bages` VALUES ('11', 'Security+');
INSERT INTO `bages` VALUES ('12', 'Organizer+');
