/*
Navicat MySQL Data Transfer

Source Server         : vm-acig-2[gfadmin_db]
Source Server Version : 50546
Source Host           : 46.173.220.71:3306
Source Database       : gf_reg

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2015-12-11 17:51:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES ('9', '1');
INSERT INTO `users_groups` VALUES ('10', '3');
INSERT INTO `users_groups` VALUES ('11', '1');
INSERT INTO `users_groups` VALUES ('12', '1');
INSERT INTO `users_groups` VALUES ('13', '2');
INSERT INTO `users_groups` VALUES ('14', '2');
INSERT INTO `users_groups` VALUES ('15', '2');
