/*
Navicat MySQL Data Transfer

Source Server         : vm-acig-2[gfadmin_db]
Source Server Version : 50546
Source Host           : 46.173.220.71:3306
Source Database       : gf_reg

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2015-12-11 17:50:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `cat_id` tinyint(3) NOT NULL,
  `cat_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Участники');
INSERT INTO `categories` VALUES ('2', 'Участники En');
INSERT INTO `categories` VALUES ('3', 'СМИ');
INSERT INTO `categories` VALUES ('4', 'Волонтеры');
INSERT INTO `categories` VALUES ('5', 'Обслуж. перс.');
