/*
Navicat MySQL Data Transfer

Source Server         : vm-acig-2[gfadmin_db]
Source Server Version : 50546
Source Host           : 46.173.220.71:3306
Source Database       : gf_reg

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2015-12-11 17:51:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for throttle
-- ----------------------------
DROP TABLE IF EXISTS `throttle`;
CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of throttle
-- ----------------------------
INSERT INTO `throttle` VALUES ('1', '2', '127.0.0.1', '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('2', '9', '127.0.0.1', '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('3', '11', '127.0.0.1', '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('4', '12', '89.207.88.118', '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('5', '9', '31.44.90.170', '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('6', '9', '185.19.20.247', '0', '0', '0', '2015-11-18 17:44:04', null, null);
INSERT INTO `throttle` VALUES ('7', '13', '176.15.111.92', '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('8', '12', '93.80.6.52', '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('9', '15', '46.188.121.170', '0', '0', '0', null, null, null);
