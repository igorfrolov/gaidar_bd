<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>lsfkdjl</title>
	<script type="text/javascript" src="/scripts/dist/lib/min/my.lib.min.js"></script>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
	<link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME STYLES -->
	<link href="../../assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
	<link href="../../assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
	<link id="style_color" href="../../assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
	<link href="../../assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/packages/css/simplePagination.css">
	<link rel="stylesheet" href="css/main.css">

</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">
<!--Header-->
<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner">
		<div class="page-logo">
			<img src="http://www.gaidarforum.ru/images/logo2016.png" alt="">
		</div>
		<div class="page-head">
	    	<div class="page-toolbar " style="margin:20px">
				<div class="btn-group btn-theme-panel pull-right">
					<a title="Выход" href="/logout" class="btn dropdown-toggle">
						<i class="icon-logout"></i>
					</a>
				</div>
	    	</div>
	    </div>

    </div>
</div>
<!--Content-->
<div class="page-container">
    <!-- Side-bar-->
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse sidebar-menu">

        </div>
    </div>
    <div class="page-content-wrapper">
        <div class="page-content" id="page-content">
			<div class="page-content-container-js">

			</div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>

</div>
    </div>
</div>
{{-- Modal --}}
<div id="dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
    <button style="margin:10px"  onClick="$('#dialog').modal('hide')" type="button" class="btn btn-primary ">x</button>
      <!-- dialog body -->
      <div class="modal-body">

      </div>
      <!-- dialog buttons -->
    </div>
  </div>
</div>
</body>
{{--!  --}}


{{-- TEMPLATES --}}

	{{--  --}}

	{{-- Mail tpl --}}
	<script type="text/template" id="mail-tpl">
		<div class="row">
	        <div class="col-md-12">
	         	<form class="form-horizontal">
					 <div class="form-group">
				          <label class="col-md-4 control-label" for="name">Тема письма</label>
				          <div class="col-md-4">
				            <input id="subject"  name="url" type="text" value="Гайдаровский форум" class="form-control input-md">
				      	  </div>
				     </div>
				     <div class="form-group">
				          <label class="col-md-4 control-label" for="name">Текст письма</label>
				          <div class="col-md-6">
				            <textarea id="message" class="form-control" rows="6"></textarea>
				          </div>
			        </div>
				</form>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary">Отправить</button>
	    </div>
	</script>

	{{-- filter-wrapper --}}
	<script type="text/template" id="filters-wrapper-tpl">
		<div class="form-horizontal">
			<div class="form-body">
				<div class="form-group">
			        <div class="col-md-9 filters-container">
					</div>
				</div>
			</div>
		</div>
	</script>

	{{-- filter item --}}
	<script type="text/template" id="filter-item-tpl">
		<div class="col-sm-offset-2  col-sm-10	">
			<div class="checkbox">
					<label>
						<input type="checkbox"  id="checkboxWarning" value="<%-name%>">
						<%- title%>
					</label>
			</div>
		</div>
	</script>

	{{-- sidebar-item --}}
	<script type="text/template" id="sidebar-menu-item-tpl">
		<a href="javascript:;" class=" nav-link nav-toggle">
			<%- title %>
			<ul class="sub-menu"></ul>
		</a>
	</script>

	{{-- paginate --}}
	<script type="text/template" id="users-table-layout-tpl">
		<div class="table-content"></div>
		<div class="table-paginator"></div>
	</script>

	{{-- editUser --}}
	<script type="text/template" id="edit-person-tpl">
	        <div class="col-md-12">
	         	<form class="form-horizontal">

 						<%

							var filtersList = app.request('get:filters').where({'active':true});

							for (var i = 0; i < filtersList.length; i++) {
								if(filtersList[i].get('name')!='user_f_116'){

									print(
										  '<div class="form-group">'+
											  '<label class="col-md-4 control-label" for="name">'+filtersList[i].get('title')+'</label>'+
									          '<div class="col-md-4">'+
									            '<input id="subject" name="'+filtersList[i].get('name')+'" type="text" value="'+components[filtersList[i].get('name')]+'" class="form-control input-md">'+
									      	  '</div>'+
							      	      '</div>'
							      	      );

								}
							}

						%>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn save-user btn-primary">Сохранить</button>
		    </div>
	</script>


	{{-- tableTpl --}}
	<script type="text/tempalte" id="vendors-table-tpl">
            <div class="row">
				<div class="col-md-12">
					<div class="portlet box red">
						<div class="portlet-title">
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div id="light-pagination" class="pagination"></div>
									</div>
									<div class="col-md-6">
									</div>
								</div>
							</div>
							<table class="table table-striped table-hover table-bordered" id="user_table">
							<thead>
							<tr>
								<td></td>
								<%
									var filtersList = app.request('get:filters').where({'active':true});
									for (var i = 0; i < filtersList.length; i++) {
										print('<td>'+filtersList[i].get('title')+'</td>');
									};

								%>
							</tr>
							</thead>
							<tbody>

							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
	</script>

	{{-- table Item --}}
	<script type="text/tempalte" id="vendors-table-item-tpl">
				<td>
	                <!-- DOC: Remove "hide" class to enable the page header actions -->
	                <div class="page-actions">
	                    <div class="btn-group">
	                        <button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
	                            <span class="hidden-sm hidden-xs">Действия&nbsp;</span>
	                            <i class="fa fa-angle-down"></i>
	                        </button>
	                        <ul class="dropdown-menu" role="menu">
	                            <li>
	                                <a class="send-mail" href="javascript:;">
	                                    <i class="icon-envelope"></i> Отправить сообщение </a>
	                            </li>
	                            <li>
	                                <a class="edit-user" href="javascript:;">
	                                    <i class="icon-pencil"></i> Редактировать</a>
	                            </li>
	                            <li>
	                                <a class="change-badge" href="javascript:;">
	                                    <i class="icon-user"></i> Сменить бейдж(скоро) </a>
	                            </li>
	                        </ul>
	                    </div>
	                </div>
	            </td>
				<%

					var filtersList = app.request('get:filters').where({'active':true});



					for (var i = 0; i < filtersList.length; i++) {

						if(filtersList[i].get('name')==='user_f_116'){

							print('<td><img src="'+components[filtersList[i].get('name')]+'" style="width:80px" alt=""></td>')

						}

						else if(filtersList[i].get('name')==='user_stiker') {
							print('<td><i class="icon-user user_stiker-'+components[filtersList[i].get('name')]+'" style="font-size:25px;line-height:80px;"></td>')
						}

						else {
							print('<td>'+components[filtersList[i].get('name')]+'</td>');
						}
					};

				%>
	</script>

{{-- start app --}}
<script type="text/javascript" src="/scripts/dist/normal/index.js"></script>
	<script type="text/javascript">
		app.start()
	</script>
</html>
