@if( ! Sentry::check() )

<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Metronic | Login Options - Login Form 4</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/pages/css/login-soft.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="../../assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="../../assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
		<img src="http://www.gaidarforum.ru/images/logo2016.png" style="width:600px" alt=""/>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
				{{ Form::open(array('action' => 'RegisterController@login','class' => 'login-form','method' => 'post')) }}
					<h3 class="form-title">Вход в систему</h3>

					@if (!$errors->isEmpty())
						<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
								<p>{{ $error }}</p>
							@endforeach
						</div>
					@endif

					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Username</label>
						<div class="input-icon">
							{{ Form::text('email', null, array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Email')) }}
						</div>
					</div>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Password</label>
						<div class="input-icon">
							{{ Form::password('password', array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Password')) }}
						</div>

					</div>
					<div class="form-actions">
						<div class="col-xs-8">
							<div class="checkbox icheck">
								<label>
									{{ Form::checkbox('remember-me', 1) }} Запомни меня
								</label>
							</div>
						</div><!-- /.col -->
						<div class="col-xs-4">
							{{ Form::submit('Войти', array('class' => 'btn  pull-right')) }}
						</div><!-- /.col -->
					</div>

				{{ Form::close() }}
	<!-- END LOGIN FORM -->
</div>
<!-- END LOGIN -->

<script src="../../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {
  Metronic.init(); // init metronic core components
       // init background slide images
       $.backstretch([
        "../../assets/admin/pages/media/bg/1.jpg",
        "../../assets/admin/pages/media/bg/2.jpg",
        "../../assets/admin/pages/media/bg/3.jpg",
        "../../assets/admin/pages/media/bg/4.jpg"
        ], {
          fade: 1000,
          duration: 8000
    }
    );
});
</script>
</body>
</html>

@else
	{{ Redirect::to('/'); }}
@endif
