
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin Panel</title>
</head>
<script type="text/javascript" src="/packages/bower_components/jquery/dist/jquery.min.js"></script>
{{HTML::style('/admin/css/admin_register.css')}}
<link rel="stylesheet" href="/admin/ModalWindowEffects/css/component.css">
<body>
<div class="container">

		{{-- модальное окно редактирования пользователя --}}
		<div class="md-modal md-effect-8" id="update-user">
			<div class="md-content">
					<h3>Редактирование</h3>
					<div>
					<div class="loading-wrapper absolute-wrapper">
						<div class="loading">
							<img src="/images/entities/loader.GIF" alt="">
						</div>
					</div>
						<form action="register/update_user" id="update_user" method="post">
							<input type="text" name="first_name" ><label for="first_name">Имя</label>
							<input type="text" name="sur_name" ><label for="sur_name">Фамилия</label>
							<input type="text" name="phone_number" ><label for="phone_number">Телефон</label>
							<input type="text" name="email" ><label for="email">e-mail</label>
							<input type="password" name="password" autocomplete="off" ><label for="password">Пароль</label>
							<input type="hidden" name="id" value="">
							<select name="group_id" class="group_select">
								<option value="">Выберите группу</option>
							</select>
							<label for="group_id">Группа</label>
							<input class="user_update_form_submit" type="submit" value="Продолжить">
						</form>
					</div>
			</div>
		</div>

		{{-- Модальное окно для добавления пользователя --}}
		<div class="md-modal md-effect-8" id="reg_user">
			<div class="md-content">
					<h3>Добавление пользователя</h3>
					<div>
						<form action="registration_user" id="reg_user_form" method="post">
							<input type="text" name="email" autocomplete ="off" ><label for="email">e-mail</label>
							<input type="password" name="password" autocomplete="off" ><label for="password">Пароль</label>
							<input type="text" name="first_name" ><label for="first_name">Имя</label>
							<input type="text" name="sur_name" ><label for="sur_name">Фамилия</label>
							<input type="text" name="phone_number" ><label for="phone_number">Телефон</label>
							<select name="group_id" class="group_select">
								<option value="">Выберите группу</option>
							</select>
							<label for="group_id">Группа</label>
							<input class="user_register_form_submit" type="submit" value="Продолжить">
						</form>
					</div>
			</div>
		</div>
		{{-- Модальное окно для удаления пользователя --}}
		<div class="md-modal md-effect-8 md-delete" id="delete-user">
			<div class="md-content">
					<h3>Действительно хотите удалить пользователя?</h3>
					<div>
						<p class="delete_accepted delete_admin_user">Да<p>
						<p class="delete_declined delete_admin_user">Нет<p>
					</div>
			</div>
		</div>
		{{-- !Модальные окна --}}
			<header class="clearfix">
				<h1>Панель Администрации</h1>
			</header>
			<div class="main">
				<div class="users left">
					<button title="Добавить нового пользователя" class="md-trigger add_user button" data-modal="reg_user">
						<img src="/images/entities/plus.png" alt="">
					</button>
					<div class="title">
						<h3>Пользователи</h3>
						{{-- <a class="md-trigger" data-modal="reg-new">Добавить прользователя</div> --}}
					</div>
					<table>
						<thead>
							<tr>
								<td>№</td>
								<td>Имя</td>
								<td>Фамилия</td>
								<td>Статус</td>
								<td>Телефон</td>
								<td>e-mail</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							{{-- js content.File-admin.js line 25 --}}
						</tbody>
					</table>
					<div class="loading-wrapper">
						<div class="loading">
							<img src="/images/entities/loader.GIF" alt="">
						</div>
					</div>
				</div>
				<div class="users_groups left">
					<button title="Добавить новую группу пользователей" class="add_users_group button">
						<img src="/images/entities/plus.png" alt="">
					</button>
					<div class="title">
						<h3>Группы пользователей</h3>
						{{-- <a class="md-trigger" data-modal="reg-new">Добавить прользователя</div> --}}
					</div>
					<table>
						<thead>
							<tr>
								<td>№</td>
								<td>Название</td>
								<td>Описание</td>
							</tr>
						</thead>
						<tbody>
							{{-- js content.File-admin.js line 15 --}}
						</tbody>
					</table>
					<div class="groups-loading-wrapper">
						<div class="loading">
							<img src="/images/entities/loader.GIF" alt="">
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="events">
					<button title="Добавить новое мероприятие" class="add_events button">
						<img src="/images/entities/plus.png" alt="">
					</button>
					<div class="title">
						<h3>Мероприятия</h3>
						{{-- <a class="md-trigger" data-modal="reg-new">Добавить прользователя</div> --}}
					</div>
					<table>
						<thead>
							<tr>
								<td>№</td>
								<td>Название</td>
								<td>Title</td>
								<td>Адрес</td>
								<td>Address</td>
								<td>Дата начала регистрации</td>
								<td>Дата начала мероприятия</td>
								<td>Дата окончания мероприятия</td>
							</tr>
						</thead>
						<tbody>
							{{-- js content.File-admin.js line 15 --}}
						</tbody>
					</table>
					<div class="groups-loading-wrapper">
						<div class="loading">
							<img src="/images/entities/loader.GIF" alt="">
						</div>
					</div>
				</div>
			</div>
		</div><!-- /container -->
		<div class="md-overlay"></div><!-- the overlay element -->
<script type="text/javascript" src="/admin/js/admin.js"></script>
<script src="/admin/ModalWindowEffects/js/modalEffects.js"></script>
</body>
</html>

