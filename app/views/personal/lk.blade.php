<?php require_once(dirname(dirname(dirname(__DIR__)))).'/public/php/select_city_new.php'; ?>
<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->

<head>

	<meta charset="utf-8">

	<!--
		HTML5 Reset: https://github.com/murtaugh/HTML5-Reset
		Free to use
	-->

	<!-- Always force latest IE rendering engine (even in intranet) -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>LK</title>
	<meta name="description" content="" />

	<!-- Don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="" />

	<!-- Who owns the content of this site? -->
	<meta name="Copyright" content="" />

	<!--  Mobile Viewport
	http://j.mp/mobileviewport & http://davidbcalhoun.com/2010/viewport-metatag
	device-width : Occupy full width of the screen in its current orientation
	initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
	maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width (wrong for most sites)
	-->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Use Iconifyer to generate all the favicons and touch icons you need: http://iconifier.net -->
	<link rel="shortcut icon" href="favicon.ico" />

	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="assets/css/reset.css" />
	<link rel="stylesheet" href="assets/css/style.css" />

	<!-- Lea Verou's prefixfree (http://leaverou.github.io/prefixfree/), lets you use un-prefixed properties in your CSS files -->
	<script src="assets/js/libs/prefixfree.min.js"></script>

	<!-- This is a minimized, base version of Modernizr. (http://modernizr.com)
		  You will need to create new builds to get the detects you need. -->
	<script src="assets/js/libs/modernizr-3.2.0.base.js"></script>

	<!-- Twitter: see https://dev.twitter.com/docs/cards/types/summary-card for details -->
	<meta name="twitter:card" content="">
	<meta name="twitter:site" content="">
	<meta name="twitter:title" content="">
	<meta name="twitter:description" content="">
	<meta name="twitter:url" content="">
	<!-- Facebook (and some others) use the Open Graph protocol: see http://ogp.me/ for details -->
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content="" />

</head>

<body>

<div class="wrapper">

	<header>

		<h1><a href="/">Личныйй кабинет</a></h1>

		<nav>

			<ol>
				<li><a href="/personal/logout" class="button">Выйти</a></li>
			</ol>

		</nav>

	</header>

		<form action='/users/update' method='post' enctype='multipart/form-data'>

    <span class="label-form">Личные данные</span>

    <div class="row left">
        <label class="input-label">Фамилия</label>
        <input type='text' name='user_f_103' value='' placeholder="Фамилия*" id="surname" tabindex="1" />
    </div>

    <div class="row left">
        <label class="input-label">Транслитерация фамилии</label>
        <input type='text' name='user_f_158' value='' id="latin-surname" placeholder="Транслитерация фамилии*" />
    </div>

    <div class="row left">
        <label class="input-label">Имя</label>
        <input type='text' name='user_f_104' value='' id="name" placeholder="Имя*"  tabindex="2" />
    </div>

    <div class="row left">
        <label class="input-label">Транслитерация имени</label>
        <input type='text' name='user_f_159' value='' id="latin-name" placeholder="Транслитерация имени*" />
    </div>

    <div class="row left clearfix">
        <label class="input-label">Отчество</label>
        <input type='text' id="patronymic" name='user_f_105' value='' placeholder="Отчество*" tabindex="3"  />
    </div>

    <div class="margin"></div>

    <div class="row left select-box">
        <label class="select-label">Страна</label>
        <select name='user_f_106'>
            <option>Россия</option>
            <option>Белоруссия</option>
            <option>Украина</option>
            <option>Казахстан</option>

        </select>
    </div>

    <div class="row left select-box">
        <label class="select-label">Гражданство</label>
        <select name='user_f_172'>
            <option>Россия</option>
            <option>Белоруссия</option>
            <option>Украина</option>
            <option>Казахстан</option>
        </select>
    </div>

    <div class="margin"></div>

    <div class="row left select-box city">
        <label class="select-label">Город</label>
        <select name='user_f_108'>
            <option></option>
            <?php foreach($city as $value) { echo '<option>'. $value .'</option>'; } ?>
        </select>
    </div>

    <div class="nocity">Не нашли свой город? <a href="#" id="man">Воспользуйтесь ручной формой ввода.</a></div>

    <div id="man-block">
        <div class="row left">
            <label class="input-label">Доп. поле город</label>
            <input type='text' id='adcity' name='user_f_180' value='' />
        </div>
    </div>

    <span class="label-form">Паспортные данные</span>

    <div class="row left">
        <label class="input-label">Серия паспорта</label>
        <input type='text' name='user_f_173' value=''  id="passportS"  placeholder="Серия паспорта*" tabindex="8"  />
    </div>

    <div class="row left">
        <label class="input-label">Номер паспорта</label>
        <input type='text' name='user_f_109' value='' id="passportN" placeholder="Номер паспорта*" tabindex="9"  />
    </div>

    <div class="row left checkbox">
        <label>Пол* </label>
        <input type='radio' name='user_f_177' value='Мужской' tabindex="13" checked />Мужской
        <input type='radio' name='user_f_177' value='Женский' tabindex="13"  />Женский
    </div>

    <div class="row left">
        <label class="input-label">Дата рождения</label>
        <input type='text' name='user_f_113' value=''  id="date-b"  placeholder="Дата рождения*"  tabindex="14"  />
    </div>

    <div class="row left">
        <label class="input-label">Место рождения</label>
        <input type='text' id="pbirth" name='user_f_121' value='' placeholder="Место рождения*"  tabindex="15"/>
    </div>

    <span class="label-form">Дополнительная информация</span>

    <div class="row left">
        <label class="input-label">Организация</label>
        <input type='text' name='user_f_114' value='' id="organization" placeholder="Организация*" tabindex="16"   />
    </div>

    <div class="row left">
        <label class="input-label">Организация на английском</label>
        <input type='text' name='user_f_161' value='' id="latin-organization" placeholder="Организация (на английском)*"  />
    </div>

    <div class="row left">
        <label class="input-label">Должность</label>
        <input type='text' name='user_f_115' value='' id="position" placeholder="Должность*" tabindex="17"  />
    </div>

    <div class="row left">
        <label class="input-label">Должность на английском</label>
        <input type='text' name='user_f_162' value='' id="latin-position"  placeholder="Должность (на английском)*"  />
    </div>

    <div class="row left file">
        <label>Фото*</label>
        <input type='file' name='user_f_116' value=''  tabindex="18" />
        <div style="float: right" class="text-photo">
            <p>Фотография должна быть предоставлена:<br />
            <ul>
                <li>В цвете</li>
                <li>Размером 30 на 40 мм</li>
                <li>С четким изображением лица строго в анфас без головного убора</li>
                <li>Размер изображения овала лица на фотографии должен занимать не менее<br /> 80 процентов от размера фотографии</li>
                <li>Задний фон светлее изображения лица, ровный, без полос, пятен и  посторонних предметов</li>
            </ul>
            </p>

        </div>
    </div>

    <span class="label-form">Контактная информация</span>

    <div class="row left">
        <label class="input-label">Контактное лицо</label>
        <input id="contact" type='text' class="wide" name='user_f_117' value='' placeholder="Контактное лицо*" tabindex="22" />
    </div>

    <div class="row left">
        <label class="input-label">E-mail адрес</label>
        <input  id="email" type='text' name='su_email_reg' value='' placeholder="E-mail*" tabindex="23" />
    </div>

    <div class="row left">
        <label class="input-label">Телефон</label>
        <input id="mob" type='text' name='su_pers_mob_phone' id="homephone" placeholder="Пример: +7 (Код города) 169 0000*" value='' />
    </div>

    <div class="row wide left">
        <label class="input-label">Доп. моб. телефон</label>
        <input type='text' name='user_f_118' value='' id="phone" placeholder="Моб. телефон"  />
    </div>

    <div class="row wide left">
        <label class="input-label">Пароль</label>
        <input type='text' name='su_pass' value='' id="pass" placeholder="PASSWORD"  />
    </div>

    <div class="row wide left">
        <label class="input-label">Подтверждение пароля</label>
        <input type='text' name='su_pass_confirm' value='' id="pass" placeholder="CONFIRM PASSWORD"  />
    </div>

    <div class="margin"></div>

    <div class="row submit">
        <input name="sub" id="sub" disabled="true" type='submit' value='Сохранить' tabindex="25" />
    </div>

</form>

	
	
	<footer>

		<p><small>&copy; Copyright Your Name Here 2014. All Rights Reserved.</small></p>

	</footer>

</div>

</body>
</html>


