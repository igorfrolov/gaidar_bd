@if( Sentry::check() )

    @extends('admin.layout.dashboard')

@section('content')
@endsection

@else
    {{ Redirect::to('login'); }}
@endif