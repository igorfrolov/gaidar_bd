<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset("/dist/img/userdef.png") }}" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p>{{ $user->first_name }} {{ $user->sur_name }}</p>
        <!-- Status -->
        <a href="#">
          @foreach($groups as $group)
            {{ $group->name }}
          @endforeach
        </a>
      </div>
    </div>

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Поиск..."/>
        <span class="input-group-btn">
          <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->

    <!-- Sidebar Menu -->
    @include('admin.menu.sidebar')
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>