@if( Sentry::check() )

@extends('admin.layout.dashboard')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Последние добавленные мероприятия</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Название En.</th>
                            <th>Адрес</th>
                            <th>Адрес En.</th>
                            <th>Дата начала регистрации</th>
                            <th>Дата начала мероприятия</th>
                            <th>Дата окончания мероприятия</th>
                            <th>Менеджер</th>
                        </tr>
                        @foreach($events as $event)
                            <tr>
                                <td>{{ $event->id }}</td>
                                <td>{{ $event->title }}</td>
                                <td>{{ $event->title_en }}</td>
                                <td>{{ $event->address }}</td>
                                <td>{{ $event->address_en }}</td>
                                <td>{{ $event->register_at }}</td>
                                <td>{{ $event->begin_at }}</td>
                                <td>{{ $event->end_at }}</td>
                                <td>NULL</td>
                            </tr>
                        @endforeach
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-md-8'>
            <!-- Box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Зарегистрованные пользователи</h3>
                    {{--<div class="box-tools pull-right">--}}
                        {{--<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>--}}
                        {{--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>--}}
                    {{--</div>--}}
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th style="width:3%;text-align: center">ID</th>
                            <th>Имя</th>
                            <th>Фамилия</th>
                            <th>Уровень доступа</th>
                            <th>Телефон.</th>
                            <th>Email</th>
                            <th style="width:5%;text-align: center">Активирован</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($users as $curr_user)
                            <tr>
                                <td  style="text-align: center">{{ $curr_user->id }}</td>
                                <td>{{ $curr_user->first_name }}</td>
                                <td>{{ $curr_user->sur_name }}</td>
                                <td>{{ $curr_user->name }}</td>
                                <td>{{ $curr_user->phone }}</td>
                                <td>{{ $curr_user->email }}</td>
                                {{--<td>{{ $user->begin_at }}</td>--}}
                                <td style="text-align: center"><a href="/regadmin/user/{{ $curr_user->id }}/changeactive" ><i class="fa fa-check-circle text-success"></i></a></td>
                                <td style="width: 5%;text-align: center;"><a href="/regadmin/user/{{ $curr_user->id }}/profile" ><i class="fa fa-pencil"></i></a></td>
                                <td style="width: 5%;text-align: center;"><a href="/regadmin/user/{{ $curr_user->id }}/delete"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        @endforeach
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-md-4'>
            <!-- Box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Группы пользователей</h3>
                    {{--<div class="box-tools pull-right">--}}
                    {{--<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>--}}
                    {{--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>--}}
                    {{--</div>--}}
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Описание</th>
                        </tr>
                        @foreach($levels as $level)
                            <tr>
                                <td>{{ $level->id }}</td>
                                <td>{{ $level->name }}</td>
                                <td>{{ $level->Explane }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection

@else
    {{ Redirect::to('login'); }}
@endif