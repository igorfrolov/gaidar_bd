<ul class="sidebar-menu">
    <li class="header">Меню</li>
    <!-- Optionally, you can add icons to the links -->

        <li @if(Request::url() === 'http://regapp-loc/regadmin')class="active"@endif><a href="/regadmin"><span>Панель виджетов</span></a></li>
        <li @if(Request::url() === 'http://regapp-loc/regadmin/events')class="active"@endif><a href="/regadmin/events"><span>Мероприятия</span></a></li>
        <li @if(Request::url() === 'http://regapp-loc/regadmin/users')class="active"@endif><a href="/regadmin/users"><span>Пользователи</span></a></li>
        <li @if(Request::url() === 'http://regapp-loc/regadmin/participants')class="active"@endif><a href="/regadmin/participants"><span>Участники</span></a></li>

</ul>