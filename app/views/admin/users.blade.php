@if( Sentry::check() )

    @extends('admin.layout.dashboard')

@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Последние добавленные мероприятия</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th style="width: 3%;text-align: center;">
                                <label>
                                    <input type="checkbox" class="minimal"/>
                                </label>
                            </th>
                            <th style="width: 2%;text-align: center;">ID</th>
                            <th>Имя</th>
                            <th>Фамилия</th>
                            <th>Уровень доступа</th>
                            <th>Телефон.</th>
                            <th>Email</th>
                            <th style="width: 5%;text-align: center;">Активирован</th>
                            <th style="width: 5%;text-align: center;">Редактировать</th>
                            <th style="width: 5%;text-align: center;">Удалить</th>
                        </tr>
                        @foreach($users as $curr_user)
                            <tr>
                                <td style="width: 3%;text-align: center;">
                                    <label>
                                        <input type="checkbox" class="minimal"/>
                                    </label>
                                </td>
                                <td style="width: 2%;text-align: center;">{{ $curr_user->id }}</td>
                                <td>{{ $curr_user->first_name }}</td>
                                <td>{{ $curr_user->sur_name }}</td>
                                <td>{{ $curr_user->name }}</td>
                                <td>{{ $curr_user->phone }}</td>
                                <td>{{ $curr_user->email }}</td>
                                {{--<td>{{ $user->begin_at }}</td>--}}
                                <td style="width: 5%;text-align: center;"><a href="/regadmin/user/{{ $curr_user->id }}/changeactive" ><i class="fa fa-check-circle text-success"></i></a></td>
                                <td style="width: 5%;text-align: center;"><a href="/regadmin/user/{{ $curr_user->id }}/profile" ><i class="fa fa-pencil"></i></a></td>
                                <td style="width: 5%;text-align: center;"><a href="/regadmin/user/{{ $curr_user->id }}/delete"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        @endforeach
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection

@else
    {{ Redirect::to('login'); }}
@endif