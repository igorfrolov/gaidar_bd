<?php

class Events extends Eloquent{

	static function getListEvents()
	{
		$eventslist =DB::table('events')
							->select('id','title','title_en','address','address_en','register_at','begin_at', 'end_at')
							->get();
		return $eventslist;
	}

	static function getEventByUser($id)
	{
		$event =DB::table('events_has_users')
							->where('events_has_users.users_id', '=', $id)
							->join('events', 'events.id', '=',  'events_has_users.events_id')
							->select('events.id','events_has_users.users_id','events.title','events.title_en','events.address','events.address_en','events.register_at','events.begin_at', 'events.end_at')
							->get();
		return $event;
	}

	
}
