<?php
/**
 * Created by PhpStorm.
 * User: a.evseev
 * Date: 16.11.2015
 * Time: 14:44
 */
class Export extends Eloquent{

    protected $table = 'sb_site_users';

    protected static $viewFields = [ 'su_id', 'su_last_ip', 'su_email', 'su_login', 'su_reg_date', 'su_last_date', 'su_active_date', 'su_domains', 'su_status', 'su_pers_mob_phone', 'su_pers_sex', 'su_mail_lang', 'su_mail_status', 'user_f_103', 'user_f_104', 'user_f_105', 'user_f_106', 'user_f_107', 'user_f_108', 'user_f_109', 'user_f_110', 'user_f_111', 'user_f_112', 'user_f_113', 'user_f_114', 'user_f_115', 'user_f_116', 'user_f_117', 'user_f_118', 'user_f_121', 'user_f_158', 'user_f_159', 'user_f_161', 'user_f_162', 'user_f_172', 'user_f_173', 'user_f_177', 'user_status', 'user_stiker', 'name_application_zip', 'name_application_csv', 'name_application', 'send_message', 'solve', 'skip_to_event'];

    public static function getAll()
    {

        return Export::select( self::$viewFields )
//            ->join('sb_catlinks', 'sb_site_users.su_id', '=', 'sb_catlinks.link_el_id')
            ->limit(100)
            ->get()
            ->toArray();
    }

    public static function getByTimestamp($timestamp){
         $result = Export::select( self::$viewFields )//
            ->where( 'su_last_date', '>', $timestamp )
            ->get()
            ->toArray();

        return $result;
    }
}