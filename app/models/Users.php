<?php
class Users extends Eloquent{
	
	protected $table = 'sb_site_users';

	protected static $requireFields = [ 'su_id', 'su_last_ip', 'su_pass', 'su_email', 'su_login', 'su_reg_date', 'su_last_date', 'su_active_date', 'su_domains', 'su_status', 'su_pers_mob_phone', 'su_pers_sex', 'su_mail_lang', 'su_mail_status', 'user_f_103', 'user_f_104', 'user_f_105', 'user_f_106', 'user_f_107', 'user_f_108', 'user_f_109', 'user_f_110', 'user_f_111', 'user_f_112', 'user_f_113', 'user_f_114', 'user_f_115', 'user_f_116', 'user_f_117', 'user_f_118', 'user_f_121', 'user_f_158', 'user_f_159', 'user_f_161', 'user_f_162', 'user_f_172', 'user_f_173', 'user_f_177', 'user_status', 'user_stiker', 'name_application_zip', 'name_application_csv', 'name_application', 'send_message', 'solve', 'skip_to_event'
    ];

    protected static $viewFields = [ 'su_id', 'su_last_ip', 'su_email', 'su_login', 'su_reg_date', 'su_last_date', 'su_active_date', 'su_domains', 'su_status', 'su_pers_mob_phone', 'su_pers_sex', 'su_mail_lang', 'su_mail_status', 'user_f_103', 'user_f_104', 'user_f_105', 'user_f_106', 'user_f_107', 'user_f_108', 'user_f_109', 'user_f_110', 'user_f_111', 'user_f_112', 'user_f_113', 'user_f_114', 'user_f_115', 'user_f_116', 'user_f_117', 'user_f_118', 'user_f_121', 'user_f_158', 'user_f_159', 'user_f_161', 'user_f_162', 'user_f_172', 'user_f_173', 'user_f_177', 'user_status', 'user_stiker', 'name_application_zip', 'name_application_csv', 'name_application', 'send_message', 'solve', 'skip_to_event'
    ];

	protected static $uniqueField = ['user_f_109'];

	public $timestamps = false;	

	public static function getAll()
	{
		$per_page = Input::get('per_page');

		return Users::select( self::$viewFields )
		->join('sb_catlinks', 'sb_site_users.su_id', '=', 'sb_catlinks.link_el_id')
		->paginate($per_page);
	}
	public static function getLastId()
	{
		return Users::select( 'su_id' )
			->orderBy('su_id', 'desc')
			->first()->su_id;
	}

	public static function update_user($id,$inputs)
	{

		// inputs
		$second_name = $inputs['user_f_103'];

		return  Users::select(
		'solve', 'send_message', 'su_id', 'user_status', 'name_application', 'user_stiker', 'skip_to_event', 'user_f_103', 'user_f_158', 'user_f_104', 'user_f_159', 'user_f_105', 'user_f_172', 'user_f_106', 'user_f_107', 'user_f_182', 'user_f_108', 'user_f_180', 'user_f_173', 'user_f_109', 'user_f_110', 'user_f_111', 'user_f_112', 'user_f_177', 'user_f_113', 'user_f_121', 'user_f_114', 'user_f_161', 'user_f_115', 'user_f_162', 'user_f_116', 'user_f_166', 'user_f_167', 'user_f_168', 'user_f_117', 'su_email', 'su_pers_mob_phone', 'su_pers_mob_phone', 'user_f_118', 'link_cat_id'
		)
		->join('sb_catlinks', 'sb_site_users.su_id', '=', 'sb_catlinks.link_el_id')
		->where('su_id','=',$id)
		// ->get();
		->update(Input::all());

	}

	/**
     * Проверка на существование пользователя с таким же email,
     * если находит вернет Объект содержащий пользователей;
     * если такого не существует вернет FALSE.
     *
     * @param $requestEmail
     *
     * @return bool
     */
    public static function check( $requestEmail ){

        $result = Users::select( self::$viewFields )
            ->join('sb_catlinks', 'sb_site_users.su_id', '=', 'sb_catlinks.link_el_id')
            ->where('su_email', '=', $requestEmail )
            ->get();

        if(count($result)) {
            return $result;
        }else{
            return FALSE;
        }
    }
    public static function register( array $credentials = []){

        $result = Users::insert($credentials);

        return $result;
    }

	public static function checkpass ( $passNumber ){
		$result = Users::select( self::$uniqueField )
		->where('user_f_109', '=', $passNumber )
		->get();

		if(count($result)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public static function auth ( array $credentials = [] ) {
		if(isset($credentials['login']) && isset($credentials['password'])){
			$result = Users::where( 'su_login', '=', $credentials['login'] )
				->where( 'su_pass', '=', md5($credentials['password']) )
				->first()->su_login;
			if(isset($result)){ 
				$_SESSION['login']=$result; 
				return TRUE;
			}
		}
		return FALSE;
	}
	public static function checkAuth(){
		if(isset($_SESSION['login'])){
			return TRUE;
		}
		return FALSE;
	}

}
