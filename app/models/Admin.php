<?php 
class Admin extends Eloquent{

	
	public static function getAllUsers(){
		$userlist =DB::table('users')
							->join('users_groups', 'users.id', '=', 'user_id')
							->join('groups', 'group_id', '=', 'groups.id')
							->select('first_name','phone','sur_name','users.id','groups.id','post','name', 'email')
							->get();
		return $userlist;
	}

	public static function getOneUser($id){
		$user =DB::table('users')
							->where('users.id', '=', $id)
							->join('users_groups', 'users.id', '=', 'user_id')
							->join('groups', 'group_id', '=', 'groups.id')
							->select('first_name','phone','sur_name','users.id','post','name', 'email')
							->get();
		return $user;
	}

	public static function getManagers(){
		$userlist =DB::table('users')
			->join('users_groups', 'users.id', '=', 'user_id')
			->join('groups', 'group_id', '=', 'groups.id')->where('groups.id', '=', 3 )
			->select('first_name','phone','sur_name','users.id','post','name','email')
			->get();
		return $userlist;
	}


	public static function getAllGroups(){
		$groups =DB::table('groups')->get();
		return $groups;
	}



	public static function updateUserGroup($id,$groupId) {
		DB::table('users_groups')
            ->where('user_id', $id)
            ->update(array('group_id' => $groupId));
	}

	public  static function createUser(){

	}

	public  static function deleteUser($id){

	}

	
}
