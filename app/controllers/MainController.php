<?php

/**
 * Class ManagerController
 */
class MainController extends BaseController {
    /**
     * __construct
     */
    public function __construct(){

    }


    public function sendMail(){
      return 'done!';
      // Mail::raw('Текст письма', function($message)
      // {
      //     $message->from('us@example.com', 'Laravel');

      //     $message->to('foo@example.com')->cc('bar@example.com');
      // });
    }


    public function getFilters()
    {
      return array(
          'filters' => array(
              array('title'=>'Фото','name'=>'photo','active'=>true),
              array('title'=>'Бейдж','name'=>'user_stiker','active'=>true),
              array('title'=>'Фамилия','name'=>'sur_name','active'=>true),
              array('title'=>'Имя','name'=>'Name','active'=>true),
              array('title'=>'Отчество','name'=>'Patronimic','active'=>true),
              array('title'=>'Е-mail','name'=>'email','active'=>true),
              array('title'=>'Телефон','name'=>'phone','active'=>true),
              array('title'=>'Серия паспорта','name'=>'passport_serial','active'=>true),
              array('title'=>'Номер паспорта','name'=>'passport_number','active'=>true),
              array('title'=>'Код подразделения','name'=>'user_f_112','active'=>true),
              array('title'=>'Кем выдан','name'=>'user_f_110','active'=>true),
              array('title'=>'Дата выдачи','name'=>'user_f_111','active'=>true),
              array('title'=>'Организация','name'=>'user_f_114','active'=>true),
            )
        );
    }

    static function getUserRole()
    {
    	return $_ENV['role'];
    }

   	public function getMenu()
   	{

   		if ($this->getUserRole() == 1) {
   			return array(
   				'status' => 'ok',
   				'code' => 0,
   				'menu' => array(
   					array('title' => 'Фильтр', 'isSubmenu' => 'false','trigger'=>'show:filter' ),
   					array('title' => 'С выбранными', 'isSubmenu'=>'true' ,'submenu' => array(
   						array('title'=>'Отправить сообщение','trigger'=>'user:send:mail'),
   						array('title'=>'Изменить бейдж','trigger'=>'change:badge')
   					)),
   					array('title' => 'Быстрый поиск', 'trigger'=>'users:fast:search' ,'isSubmenu' => 'false')
   				)
   			);
   		} else if ($this->getUserRole() == 2) {
   			return array(
   				'status' => 'ok',
   				'code' => 0,
   				'menu' => array(
   					array('title' => 'Фильтр', 'submenu' => array(
   						array('title'=>'Фото','type'=>'photo'),
   						array('title'=>'Фамилия','type'=>'sername'),
   						array('title'=>'Имя','type'=>'name'),
   						array('title'=>'Отчество','type'=>'patronimyc')
   					))
   				)
   			);
   		}
   	}

}
