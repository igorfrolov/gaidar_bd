<?php

/**
 * Class ManagerController
 */
class UserController extends BaseController {
    /**
     * __construct
     */
    public function __construct(){

    }

    public static function login(){
	$credentials = Input::only('login','password');
	if(Users::auth($credentials)){
		return View::make('personal.lk');
	}else{
		return View::make('personal.login');
	}
    }
    public static function logout(){
	unset( $_SESSION['login'] );
	return View::make('personal.login');
    }

    public static function check(){

        $requestEmail = Input::only('su_email');

        $resultCheck = Users::check($requestEmail);

        if( $resultCheck === FALSE){
            return NULL;
        }else{
            $res = [];
            foreach( $resultCheck as $item){
                $res[] = $item->su_id;
            }
            var_dump($res);
        }
    }

    public static function checkpass($passNumber){

//        $requestEmail = Input::only('su_email');

        $resultCheck = Users::checkpass($passNumber);

        if( $resultCheck === TRUE){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    static function getAll()
    {
    	return Users::getAll();
    }

    static function updateUser($id){
    	$inputs = Input::all();
    	return Users::update_user($id,$inputs);
    }
	
	public static function signUp_member(){
//        @TODO сделать проверку на существование пользователя в базе
//        self::check();
	$lastId = (int) Users::getLastId();
	$lastId++;
        $input = Input::only(
            'user_f_103',
            'user_f_158',
            'user_f_104',
            'user_f_159',
            'user_f_105',
            'user_f_106',
            'user_f_172',
            'user_f_108',
            'user_f_180',
            'user_f_173',
            'user_f_109',
            'user_f_177',
            'user_f_177',
            'user_f_113',
            'user_f_121',
            'user_f_114',
            'user_f_161',
            'user_f_115',
            'user_f_162',
            'user_f_117',
            'su_email_reg',
            'su_pers_mob_phone',
            'su_mail_lang',
            'user_f_118'
        );

        $destinationPath = dirname(dirname(__DIR__)).'/public/personal/uploads/photo';
        $fileName = $input['user_f_158'].'_'.$input['user_f_159'].'_'.$input['user_f_109'].'.'.Input::file('user_f_116')->getClientOriginalExtension();
        $file = Input::file('user_f_116')->move($destinationPath, $fileName);

        $input['user_f_116'] = 'http://reg.gaidarforum.ru/personal/uploads/photo/'.$fileName;
        $input['su_email'] = $input['su_email_reg'];
	$input['su_login'] = 'user'.$lastId;
	$input['su_pass'] = md5(12345);
        unset($input['su_email_reg']);

        $res = Users::register($input);

        var_dump($res);

    }

    public static function signUp_member_en(){
        $input = Input::only(
            'user_f_104',
            'user_f_105',
            'user_f_103',
            'user_f_106',
            'user_f_172',
            'user_f_108',
            'user_f_109',
            'user_f_177',
            'user_f_177',
            'user_f_113',
            'user_f_121',
            'user_f_114',
            'user_f_115',
            'user_f_117',
            'su_email_reg',
            'su_pers_mob_phone',
            'su_mail_lang',
            'user_f_118'
        );

        $destinationPath = dirname(dirname(__DIR__)).'/public/personal/uploads/photo';
        $fileName = $input['user_f_109'].'.'.Input::file('user_f_116')->getClientOriginalExtension();
        $file = Input::file('user_f_116')->move($destinationPath, $fileName);

        $input['user_f_116'] = 'http://reg.gaidarforum.ru/personal/uploads/photo/'.$fileName;
        $input['su_email'] = $input['su_email_reg'];
        unset($input['su_email_reg']);

        $res = Users::register($input);

        var_dump($res);
    }

    public static function signUp_service(){
        $input = Input::only(
            'user_f_103',
            'user_f_158',
            'user_f_104',
            'user_f_159',
            'user_f_105',
            'user_f_106',
            'user_f_172',
            'user_f_108',
            'user_f_180',
            'user_f_173',
            'user_f_109',
            'user_f_177',
            'user_f_177',
            'user_f_113',
            'user_f_121',
            'user_f_114',
            'user_f_161',
            'user_f_115',
            'user_f_162',
            'user_f_117',
            'su_email_reg',
            'su_pers_mob_phone',
            'su_mail_lang',
            'user_f_118'
        );

        $destinationPath = dirname(dirname(__DIR__)).'/public/personal/uploads/photo';
        $fileName = $input['user_f_158'].'_'.$input['user_f_159'].'_'.$input['user_f_109'].'.'.Input::file('user_f_116')->getClientOriginalExtension();
        $file = Input::file('user_f_116')->move($destinationPath, $fileName);

        $input['user_f_116'] = 'http://reg.gaidarforum.ru/personal/uploads/photo/'.$fileName;
        $input['su_email'] = $input['su_email_reg'];
        unset($input['su_email_reg']);

        $res = Users::register($input);

        var_dump($res);
    }

    public static function signUp_smi(){
        $input = Input::only(
            'user_f_103',
            'user_f_158',
            'user_f_104',
            'user_f_159',
            'user_f_105',
            'user_f_106',
            'user_f_172',
            'user_f_108',
            'user_f_180',
            'user_f_173',
            'user_f_109',
            'user_f_177',
            'user_f_177',
            'user_f_113',
            'user_f_121',
            'user_f_184',
            'user_f_184_cb',
            'user_f_185',
            'user_f_185_cb',
            'user_f_186',
            'user_f_186_cb',
            'user_f_114',
            'user_f_161',
            'user_f_115',
            'user_f_162',
            'user_f_117',
            'su_email_reg',
            'su_pers_mob_phone',
            'su_mail_lang',
            'user_f_118'
        );

        $destinationPath = dirname(dirname(__DIR__)).'/public/personal/uploads/photo';
        $fileName = $input['user_f_158'].'_'.$input['user_f_159'].'_'.$input['user_f_109'].'.'.Input::file('user_f_116')->getClientOriginalExtension();
        $file = Input::file('user_f_116')->move($destinationPath, $fileName);

        $input['user_f_116'] = 'http://reg.gaidarforum.ru/personal/uploads/photo/'.$fileName;
        $input['su_email'] = $input['su_email_reg'];
        unset($input['su_email_reg']);

        $res = Users::register($input);

        var_dump($res);
    }

    public static function signUp_volonteer(){
        $input = Input::only(
            'user_f_103',
            'user_f_158',
            'user_f_104',
            'user_f_159',
            'user_f_105',
            'user_f_106',
            'user_f_172',
            'user_f_108',
            'user_f_180',
            'user_f_173',
            'user_f_109',
            'user_f_177',
            'user_f_177',
            'user_f_113',
            'user_f_121',
            'user_f_114',
            'user_f_161',
            'user_f_115',
            'user_f_162',
            'user_f_117',
            'su_email_reg',
            'su_pers_mob_phone',
            'su_mail_lang',
            'user_f_118'
        );

        $destinationPath = dirname(dirname(__DIR__)).'/public/personal/uploads/photo';
        $fileName = $input['user_f_158'].'_'.$input['user_f_159'].'_'.$input['user_f_109'].'.'.Input::file('user_f_116')->getClientOriginalExtension();
        $file = Input::file('user_f_116')->move($destinationPath, $fileName);

        $input['user_f_116'] = 'http://reg.gaidarforum.ru/personal/uploads/photo/'.$fileName;
        $input['su_email'] = $input['su_email_reg'];
        unset($input['su_email_reg']);

        $res = Users::register($input);

        var_dump($res);
    }
}
