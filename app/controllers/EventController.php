<?php 
class EventController extends BaseController {

	//получение данных об администраторах
	public function getAllEvents(){
		$events = Events::getListEvents();
		$events = json_encode($events);
		return $events;
	}

	public function getEventByUser() {
		$id = $_POST['id'];
		$event = Events::getEventByUser($id);
		$event = json_encode($event);
		return $event;
	}
	
}