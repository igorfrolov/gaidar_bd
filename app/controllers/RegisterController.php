<?php
class RegisterController extends BaseController {


	public function registerView()
	{
		return View::make('layout.admin');
	}

	//аутентификация
	public  function login(){
		$inputs = Input::all();
		try
			{
			    // Login credentials
			    $credentials = array(
			        'email'    => $inputs['email'],
			        'password' => $inputs['password'],
			    );

			    // Authenticate the user
			    $user = Sentry::authenticateAndRemember($credentials, false);
			    if ($user) {
			    	return Redirect::to('/');
			    }
			}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    echo 'Login field is required.';
			}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
			    echo 'Password field is required.';
			}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
			{
			    echo 'Wrong password, try again.';
			}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    echo 'User was not found.';
			}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
			{
			    echo 'User is not activated.';
			}
		// The following is only required if the throttling is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
			{
			    echo 'User is suspended.';
			}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
			{
			    echo 'User is banned.';
			}

		catch (\Exception $e)
		{
		    return Redirect::to('/login')->withErrors(array('/login'));
		}
	}


	//ДОБАВЛЕНИЕ нового пользователя системы.
	public function register()
	{
		$inputs = Input::all();
		try
		{
		    // Create the user
		    $user = Sentry::createUser(array(
		        'email'     	=> $inputs['email'],
		        'password'  	=> $inputs['password'],
		        'first_name'	=> $inputs['first_name'],
		        'sur_name'  	=> $inputs['sur_name'],
		        'phone'  		=> $inputs['phone_number'],
		        'activated'     => true,
		    ));

		    // Find the group using the group id
		    $adminGroup = Sentry::findGroupById($inputs['group_id']);

		    // Assign the group to the user
		    $user->addGroup($adminGroup);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    echo 'Login field is required.';
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    echo 'Password field is required.';
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    echo 'User with this login already exists.';
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		    echo 'Group was not found.';
		}

	}

	//РЕДАКТИРОВАНИЕ ПОЛЬЗОВАТЕЛЯ
	public function updateUser(){
		//достаем значения из сериалайза admin.js
		$inputs =  Input::all();
		//меняем группу
		Admin::updateUserGroup($inputs['id'],$inputs['group_id']);
		//меняем данные
		try
		{

		    $user = Sentry::findUserById($inputs['id']);

		    // Update the user details
		    $user->first_name 	=  $inputs['first_name'];
		    $user->sur_name 	=  $inputs['sur_name'];
		    $user->email 		=  $inputs['email'];
		    $user->password  	=  $inputs['password'];
		    $user->phone 		=  $inputs['phone_number'];


		    // Update the user
		    if ($user->save())
		    {
		        echo 'User updated';
		    }
		    else
		    {
		        echo 'User is not updated';
		    }
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    echo 'User with this login already exists.';
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User was not found.';
		}
	}



	//ДОБАВЛЕНИЕ новой группы пользователей
	public function register_group()
	{

		$inputs = Input::all();
		try
			{
			    // Create the group
			    $group = Sentry::createGroup(array(
			        'name'       	=> $inputs['name'],
			        'permissions' 	=> array(
			        'admin' 		=> 1,
			        'users' 		=> 1,
			        ),
			    ));
			}
		catch (Cartalyst\Sentry\Groups\NameRequiredException $e)
			{
			    echo 'Name field is required';
			}
		catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
			{
			    echo 'Group already exists';
			}


	}


	//удаление пользователя
	public function deleteUser(){
		$id = $_POST['id'];
		try
			{
			    $user = Sentry::findUserById($id);

			    $user->delete();
			}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    echo 'User was not found.';
			}

	}


	//logout

	public function logout(){
		Sentry::logout();
	    return Redirect::to('/login');
	}




	//получение всех групп
	public function getAllGroups(){
		$groups = Admin::getAllGroups();
		$groups = json_encode($groups);
		return $groups;


	//получение данных об администраторах
	}public function getAllUsers(){
		$users = Admin::getAllUsers();
		$users = json_encode($users);
		return $users;
	}

	public function getOneUser() {
		$id = $_POST['id'];
		$user = Admin::getOneUser($id);
		$user = json_encode($user);
		return $user;
	}
}
 ?>
