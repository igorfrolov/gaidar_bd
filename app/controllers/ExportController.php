<?php
/**
 * Created by PhpStorm.
 * User: a.evseev
 * Date: 16.11.2015
 * Time: 14:42
 */
class ExportController extends BaseController {

//    static $file = "export.xml";

//    public $timestamp;

//    public function getTimestamp(){
//        self::$timestamp = Input::get('timestamp');
//    }

    public static function miss()
    {
        return "you are lost? :(";
    }

    public static function getAll()
    {
        return Export::getAll();
    }
//
//    public static function getByTimestamp($timestamp)
//    {
//        return Export::getByTimestamp($timestamp);
//    }

    public static function exportCsv($timestamp){

        $result = Export::getByTimestamp($timestamp);

//        echo "<pre>";
//        var_dump($result);
//        echo "</pre>";
        $filename = "csvfeed_" .$timestamp. ".csv";

        $file = fopen($filename, 'w');

        foreach ($result as $key) {
            fputcsv($file,
                array($key['su_id'],$key['user_f_106'],$key['user_f_103'],$key['user_f_104'],$key['user_f_105'],$key['user_f_172'],$key['user_f_158'],$key['user_f_159'],$key['user_f_177'],
                    $key['user_f_113'],$key['user_f_121'],$key['user_f_173'],$key['user_f_109'],$key['user_f_114'],$key['user_f_115'],$key['name_application'],$key['user_f_116']));
        }

        fclose($file);
//        $path = public_path()."\\export\\";
//        File::move($path, $file);
        return Response::download($filename);
    }

    public static function exportXml(){
            $result = self::getAll();
            $file = public_path()."\\export\\xmlfeed.xml";
        $content = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><list><persons_list>";


        echo "<pre>";
        var_dump($result);
        echo "</pre>";
        foreach ($result as $key){
            $content .="<person>
                <number>".$key['su_id']."</number>
                <country>".$key['user_f_106']."</country>
                <last_name>".$key['user_f_103']."</last_name>
                <first_name>".$key['user_f_104']."</first_name>
                <midle_name>".$key['user_f_105']."</midle_name>
                <citizenship>".$key['user_f_172']."</citizenship>
                <accreditation_number>"."����� ������������ ���"."</accreditation_number>
                <last_name_translit>".$key['user_f_158']."</last_name_translit>
                <first_name_translit>".$key['user_f_159']."</first_name_translit>
                <gender>".$key['user_f_177']."</gender>
                <birth_date>".$key['user_f_113']."</birth_date>
                <birth_place>".$key['user_f_121']."</birth_place>
                <pasport_series>".$key['user_f_173']."</pasport_series>
                <pasport_number>".$key['user_f_109']."</pasport_number>
                <job_place>".$key['user_f_114']."</job_place>
                <job>".$key['user_f_115']."</job>
                <function>".$key['name_application']."</function>
                <photo>".$key['user_f_116']."</photo>
            </person>";

        }
        $content .= "</persons_list></list>";

        File::put($file, $content);
    }

}