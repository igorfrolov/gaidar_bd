<?php

/**
 * Class ManagerController
 */
class ManagerController extends BaseController {

    private $events;
    private $users;
    private $managers;
    private $levels;

    private $user;
    private $groups;

    /**
     * __construct
     */
    public function __construct(){

        $this->events = Events::getListEvents();
        $this->users  = Admin::getAllUsers();
        $this->managers  = Admin::getManagers();
        $this->levels = Admin::getAllGroups();

        $objUser = Sentry::getUser();
        $this->user = $objUser;
        $this->groups = $objUser->getGroups();
    }

    public function manager()
    {

        return View::make('admin.index', array(
                                        'user'       => $this->user,
                                        'users'      => $this->users,
                                        'events'     => $this->events,
                                        'groups'     => $this->groups,
                                        'page_title' => 'Панель администрирования',
                                        'levels'     => $this->levels,
        ));
    }

    public function getEvents()
    {

        return View::make('admin.events', array(
            'user'       => $this->user,
            'events'     => $this->events,
            'managers'   => $this->managers,
            'groups'     => $this->groups,
            'page_title' => 'Мероприятия',
            'menu_item'  => 'active',
        ));
    }

    public function getUsers()
    {

        return View::make('admin.users', array(
            'user'       => $this->user,
            'users'      => $this->users,
            'groups'     => $this->groups,
            'page_title' => 'Пользователи системы',
            'menu_item'  => 'active',
        ));
    }

}
