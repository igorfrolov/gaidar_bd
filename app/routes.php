<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/login', function()
{
	return View::make('login');
});

Route::group(array('before'=>'auth'),function(){
	Route::get('/','ViewController@index');
});



//Регистрация администратора сиситемы
Route::get('/register','RegisterController@registerView');//вьюшка админ панели
Route::get('/register/users','RegisterController@userForm');//форма пользователей
Route::get('/register/groups','RegisterController@groupsFrom');//форма групп пользователей
Route::post('/register/update_user','RegisterController@updateUser');//редактирование пользователя
Route::post('/register/delete_user','RegisterController@deleteUser');//удаление пользователя
Route::post('/loginfoo','RegisterController@login');//аутентификация(логин)
Route::post('/registration_user','RegisterController@register');//добавление пользователя
Route::post('/registration_group','RegisterController@register_group');//добавление группы


/*Логаут*/
Route::get('/logout','RegisterController@logout');

/*--!*/
/*Получение данных об администраторах системы*/
Route::get('/getAllUsers','RegisterController@getAllUsers');
Route::get('/getAllGroups','RegisterController@getAllGroups');
Route::post('/getOneUser','RegisterController@getOneUser');
/*!--*/

/* Получение данных о мероприятиях */
Route::get('/getAllEvents','EventController@getAllEvents');
Route::post('/getEventByUser','EventController@getEventByUser');
Route::get('/regadmin','ManagerController@manager');//вьюшка админ панели
Route::get('/regadmin/events','ManagerController@getEvents');//вьюшка админ панели "Мероприятия"
Route::get('/regadmin/users','ManagerController@getUsers');//вьюшка админ панели "Пользователи"


/*Меню главной страницы*/
Route::get('/menu', 'MainController@getMenu');
Route::get('/getFilters', 'MainController@getFilters');


/*Users resourses*/
Route::get('/users/all', 'UserController@getAll');
Route::post('/users/signup/member', 'UserController@signUp_member');
Route::post('/users/signup/member/en', 'UserController@signUp_member_en');
Route::post('/users/signup/service', 'UserController@signUp_service');
Route::post('/users/signup/smi', 'UserController@signUp_smi');
Route::post('/users/signup/volonteer', 'UserController@signUp_volonteer');
Route::post('/{id}', 'UserController@updateUser');

//LK
Route::get('/personal/login', function()
{
	if(Users::checkAuth()){
        	return View::make('personal.login');
    	}
	return View::make('personal.login');
});
Route::post('/personal/login', 'UserController@login');
Route::get('/personal/logout', 'UserController@logout');
Route::get('/personal', function(){
	if(!Users::checkAuth()){
        	return View::make('personal.login');
    	}
	return View::make('personal.lk');
});

/*Send Mail*/
Route::get('/sendMail', 'MainController@sendMail');

/* Unique Fields */
Route::get('/checkpass/{passnumber}', 'UserController@checkpass');

/* Exports */
Route::get('/export/xml', 'ExportController@exportXml');
Route::get('/export/csv', 'ExportController@miss');
Route::get('/export/csv/{timestamp}', 'ExportController@exportCsv');
